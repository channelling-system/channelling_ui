# Auction-Seller-UI


Starting Application
------------

Runs the app in the development mode.
Open http://localhost:3000 to view app in the browser.

```sh
$ npm start
```

Build Application
------------

Builds the app for production to the build folder

```sh
$ npm run build
```
