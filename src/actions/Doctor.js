import axios from 'util/Api'

const doctorUrl = 'doctors';

export const getAllDoctors = async () => {
    try {
        return await axios.get(doctorUrl);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};

export const saveDoctor = async (data) => {
    try {
        return await axios.post(doctorUrl, data);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};

export const updateDoctor = async (id, data) => {
    try {
        return await axios.put(doctorUrl + '/' + id, data);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};
