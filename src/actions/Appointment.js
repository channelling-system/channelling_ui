import axios from 'util/Api'


const appointmentUrl = 'appoinments';

export const getAppointments = async () => {
    try {
        return await axios.get(appointmentUrl);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};

export const getAppointmentById = async (id) => {
    try {
        return await axios.get(appointmentUrl + '/' + id);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};

export const getAppointmentsBySessionId = async (id) => {
    try {
        return await axios.get(appointmentUrl + '/session/' + id);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};

export const updateAppointment = async (id, data) => {
    try {
        return await axios.put(appointmentUrl + '/' + id, data);
    } catch (error) {
        console.log("Error****:", error.message);
    }
};