import {
    FETCH_ERROR,
    FETCH_START,
    FETCH_SUCCESS,
    INIT_URL,
    SIGNOUT_USER_SUCCESS,
    USER_DATA,
    USER_TOKEN_SET
} from "../constants/ActionTypes";
import axios from 'util/Api'

var jwtDecode = require('jwt-decode');

export const setInitUrl = (url) => {
    return {
        type: INIT_URL,
        payload: url
    };
};

export const userSignUp = ({name, email, password}) => {
    console.log(name, email, password);
    return (dispatch) => {
        // dispatch({type: FETCH_START});
        // axios.post('auth/register', {
        //         email: email,
        //         password: password,
        //         name: name
        //     }
        // ).then(({data}) => {
        //     console.log("data:", data);
        //     if (data.result) {
        //         localStorage.setItem("token", JSON.stringify(data.token.access_token));
        //         axios.defaults.headers.common['Authorization'] = "Bearer " + data.token.access_token;
        //         dispatch({type: FETCH_SUCCESS});
        //         dispatch({type: USER_TOKEN_SET, payload: data.token.access_token});
        //         dispatch({type: USER_DATA, payload: data.user});
        //     } else {
        //         console.log("payload: data.error", data.error);
        //         dispatch({type: FETCH_ERROR, payload: "Network Error"});
        //     }
        // }).catch(function (error) {
        //     dispatch({type: FETCH_ERROR, payload: error.message});
        //     console.log("Error****:", error.message);
        // });

        // localStorage.setItem("token", JSON.stringify('token'));
        // localStorage.setItem("user", JSON.stringify('user'));
        // dispatch({type: FETCH_SUCCESS});
        // dispatch({type: USER_TOKEN_SET, payload: 'token'});

    }
};

export const userSignIn = ({username, password}) => {
    return (dispatch) => {
        dispatch({type: FETCH_START});
        axios.post('login', {
                username: username,
                password: password,
            }
        ).then(({data}) => {
            if (data) {
                localStorage.setItem("token", data.accessToken);
                localStorage.setItem("username", jwtDecode(data.accessToken).sub);
                axios.defaults.headers.common['Authorization'] = "Bearer " + data.accessToken;
                dispatch({type: FETCH_SUCCESS});
                dispatch({type: USER_TOKEN_SET, payload: data.accessToken});
            } else {
                dispatch({type: FETCH_ERROR, payload: data.error});
            }
        }).catch(function (error) {
            dispatch({type: FETCH_ERROR, payload: error.message});
            console.log("Error****:", error.message);
        });

    }
};

export const getUser = () => {
    return (dispatch) => {
        dispatch({type: FETCH_START});
        axios.post('auth/me',
        ).then(({data}) => {
            console.log("userSignIn: ", data);
            if (data.result) {
                dispatch({type: FETCH_SUCCESS});
                dispatch({type: USER_DATA, payload: data.user});
            } else {
                dispatch({type: FETCH_ERROR, payload: data.error});
            }
        }).catch(function (error) {
            dispatch({type: FETCH_ERROR, payload: error.message});
            console.log("Error****:", error.message);
        });
    }
};


export const userSignOut = () => {
    return (dispatch) => {
        dispatch({type: FETCH_START});
        // axios.post('auth/logout',
        // ).then(({data}) => {
        //   if (data.result) {
        //     dispatch({type: FETCH_SUCCESS});
        //     localStorage.removeItem("token");
        //     dispatch({type: FETCH_SUCCESS});
        //     dispatch({type: SIGNOUT_USER_SUCCESS});
        //   } else {
        //     dispatch({type: FETCH_ERROR, payload: data.error});
        //   }
        // }).catch(function (error) {
        //   dispatch({type: FETCH_ERROR, payload: error.message});
        //   console.log("Error****:", error.message);
        // });
        dispatch({type: FETCH_SUCCESS});
        localStorage.removeItem("token");
        dispatch({type: FETCH_SUCCESS});
        dispatch({type: SIGNOUT_USER_SUCCESS});

    }
};
