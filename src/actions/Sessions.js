import {
    FETCH_ERROR,
    FETCH_START,
    FETCH_SUCCESS,
    INIT_URL, SESSION_DATA,
} from "../constants/ActionTypes";
import axios from 'util/Api'

export const setInitUrl = (url) => {
    return {
        type: INIT_URL,
        payload: url
    };
};


export const getSessions = () => {
    return (dispatch) => {
        dispatch({type: FETCH_START});
        axios.get('sessions',
        ).then(({data}) => {
            console.log("sessions: ", data);
            if (data) {
                dispatch({type: FETCH_SUCCESS});
                dispatch({type: SESSION_DATA, payload: data});
            } else {
                dispatch({type: FETCH_ERROR, payload: data.error});
            }
        }).catch(function (error) {
            dispatch({type: FETCH_ERROR, payload: error.message});
            console.log("Error****:", error.message);
        });
    }
};

