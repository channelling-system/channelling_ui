import React, {useEffect} from "react";
import {createMuiTheme} from "@material-ui/core/styles";
import {ThemeProvider} from "@material-ui/styles";
import URLSearchParams from "url-search-params";
import MomentUtils from "@date-io/moment";
import {MuiPickersUtilsProvider} from "material-ui-pickers";
import {Redirect, Route, Switch} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {IntlProvider} from "react-intl";
import "assets/vendors/style";
import AppLocale from "../lngProvider";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import {setInitUrl} from "../actions/Auth";
import RTL from "util/RTL";
import {setThemeColor} from "../actions/Setting";
import AppLayout from "./AppLayout";
import axios from "../util/Api";
import customTheme from "./themes/customTheme";
import {DARK_CUSTOM} from "../constants/ThemeColors";
import {userSignOut} from "../actions";

const RestrictedRoute = ({component: Component, token, ...rest}) =>
  <Route
    {...rest}
    render={props =>
      token
        ? <Component {...props} />
        : <Redirect
          to={{
            pathname: '/signin',
            state: {from: props.location}
          }}
        />}
  />;

const App = (props) => {

  if (localStorage.getItem('token')) {
  axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('token');
}

  const dispatch = useDispatch();
  const {themeColor, darkTheme, locale, isDirectionRTL} = useSelector(({settings}) => settings);
  const {token, initURL} = useSelector(({auth}) => auth);
  const isDarkTheme = darkTheme;
  const {match, location} = props;

  axios.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    //catches if the session ended!
    if ( error.response.status === 403) {
      dispatch(userSignOut());
    }
    return Promise.reject(error);
  });

  useEffect(() => {
    window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;
    if (initURL === '') {
      dispatch(setInitUrl(props.history.location.pathname));
    }
    const params = new URLSearchParams(props.location.search);
    // if (params.has("theme-name")) {
    //   dispatch(setThemeColor(params.get('theme-name')));
    // }
    // if (params.has("dark-theme")) {
    //   dispatch(setDarkTheme());
    // }
    dispatch(setThemeColor(DARK_CUSTOM));
    const body = document.body.classList;
    body.remove(themeColor);
    body.remove('dark-theme');
    body.add(DARK_CUSTOM);
  }, [dispatch, initURL, props.history.location.pathname, props.location.search]);


  const getColorTheme = () => {
    return createMuiTheme(customTheme);
  };

  let applyTheme = createMuiTheme(customTheme);
  // if (isDarkTheme) {
  //   document.body.classList.add('dark-theme');
  //   applyTheme = createMuiTheme(darkTheme)
  // } else {
  //   applyTheme = getColorTheme(themeColor, applyTheme);
  // }

  applyTheme = getColorTheme(themeColor, applyTheme)

  if (location.pathname === '/') {
    if (token === null) {
      return (<Redirect to={'/signin'}/>);
    } else if (initURL === '' || initURL === '/' || initURL === '/signin') {
      return (<Redirect to={'/app/dashboard'}/>);
    } else {
      return (<Redirect to={initURL}/>);
    }
  }
  if (isDirectionRTL) {
    applyTheme.direction = 'rtl';
    document.body.classList.add('rtl')
  } else {
    document.body.classList.remove('rtl');
    applyTheme.direction = 'ltr';
  }

  const currentAppLocale = AppLocale[locale.locale];
  return (
    <ThemeProvider theme={applyTheme}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}>
          <RTL>
            <div className="app-main">
              <Switch>
                <RestrictedRoute path={`${match.url}app`} token={token}
                                 component={AppLayout}/>
                <Route path='/signin' component={SignIn}/>
                <Route path='/signup' component={SignUp}/>
                {/*<Route*/}
                {/*  component={asyncComponent(() => import('app/routes/extraPages/routes/404'))}/>*/}
              </Switch>
            </div>
          </RTL>
        </IntlProvider>
      </MuiPickersUtilsProvider>
    </ThemeProvider>
  );
};


export default App;
