import React from 'react';
import CustomScrollbars from 'util/CustomScrollbars';
import Navigation from "../../components/Navigation";

const SideBarContent = () => {
  const navigationMenus = [
    {
      name: 'sidebar.main',
      type: 'section',
      children: [
        {
          name: 'sidebar.main.dashboard',
          icon: 'zmdi zmdi-view-dashboard zmdi-hc-fw',
          type: 'item',
          link: '/app/dashboard'
        },
        {
          name: 'sidebar.main.myLots',
          icon: 'zmdi zmdi-account-box zmdi-hc-fw',
          type: 'item',
          link: '/app/doctors'
        },
        {
          name: 'sidebar.main.roomReservation',
          icon: 'zmdi zmdi-layers zmdi-hc-fw',
          type: 'item',
          link: '/app/sessions'
        },
        {
          name: 'sidebar.main.room',
          icon: 'zmdi zmdi-hospital zmdi-hc-fw',
          type: 'item',
          link: '/app/rooms'
        },
        {
          name: 'sidebar.main.appointments',
          icon: 'zmdi zmdi-receipt zmdi-hc-fw',
          type: 'item',
          link: '/app/appointments'
        },
      ]
    }
  ];

  return (
    <CustomScrollbars className=" scrollbar">
      <Navigation menuItems={navigationMenus}/>
    </CustomScrollbars>
  );
};

export default SideBarContent;
