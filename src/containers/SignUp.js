import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Button from '@material-ui/core/Button';
import {Link} from 'react-router-dom';
import IntlMessages from 'util/IntlMessages';
import {userSignUp} from '../actions/Auth';
import {hideMessage} from "../actions";

const SignUp = (props) => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();
    const token = useSelector(({auth}) => auth.token);


    useEffect(() => {
        if (props.message) {
            setTimeout(() => {
                dispatch(hideMessage());
            }, 3000);
        }
        if (token !== null) {
            props.history.push('/');
        }
    }, [props.message, token])

    return (
        <div
            className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
            <div className="login-content text-center">
                <div className="login-header">
                    <div className="app-logo" title="Central Hospital">
                        {/*<h2 className="jr-font-weight-bold font-italic text-primary">*/}
                        {/*    <IntlMessages id="app.name"/>*/}
                        {/*</h2>*/}
                        <img src={require("assets/images/logo.png")} alt="Central Hospital" title="Central Hospital"/>
                    </div>
                </div>

                {/*<div className="mb-4">*/}
                {/*    <h2><IntlMessages id="appModule.createAccount"/></h2>*/}
                {/*</div>*/}

                <div className="login-form">
                    <form method="post" action="/">
                        <div className="form-group">
                            <input type="text" placeholder="Name"
                                   className="form-control form-control-lg"
                                   onChange={(event) => setName(event.target.value)}/>
                        </div>
                        <div className="form-group">
                            <input type="text" placeholder="Email"
                                   className="form-control form-control-lg"
                                   onChange={(event) => setEmail(event.target.value)}/>
                        </div>
                        <div className="form-group">
                            <input type="password" placeholder="Password"
                                   className="form-control form-control-lg"
                                   onChange={(event) => setPassword(event.target.value)}/>
                        </div>
                            <Button className="bg-primary text-white py-2 mt-3" fullWidth={true}
                                    onClick={() => {dispatch(userSignUp({name, email, password}));}}>
                                <IntlMessages id="appModule.regsiter"/>
                            </Button>
                        <p className="mt-3">
                            <IntlMessages id="appModule.hvAccount"/>
                            <Link to="/signin" className="ml-1">
                                <IntlMessages id="appModule.signIn"/>
                            </Link>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    )
};


export default SignUp;
