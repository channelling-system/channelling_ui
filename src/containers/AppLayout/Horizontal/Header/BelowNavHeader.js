import React, {useState} from "react";
import {withRouter} from "react-router-dom";
import {useDispatch} from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Avatar from "@material-ui/core/Avatar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import {Dropdown, DropdownMenu, DropdownToggle} from "reactstrap";
import {toggleCollapsedNav} from "actions/Setting";
import IntlMessages from "util/IntlMessages";
import UserInfoPopup from "components/UserInfo/UserInfoPopup";

const Header = () => {

    const dispatch = useDispatch();
    const [userInfo, setUserInfo] = useState(false);


    const onUserInfoSelect = () => {
        setUserInfo(!userInfo);
    };

    const onToggleCollapsedNav = (e) => {
        dispatch(toggleCollapsedNav());
    };

    return (
        <AppBar
            className='app-main-header app-main-header-top'>
            <Toolbar className="app-toolbar" disableGutters={false}>
                <div className="d-block d-md-none pointer mr-3" onClick={onToggleCollapsedNav}>
                            <span className="jr-menu-icon">
                              <span className="menu-icon"/>
                            </span>
                </div>


                <div className="app-logo mr-2 d-none d-sm-block">
                    {/*<h3 className="jr-font-weight-bold font-italic text-white mb-0">*/}
                    {/*    <IntlMessages id="app.name"/>*/}
                    {/*</h3>*/}
                    <img src={require("assets/images/logo.png")} alt="Central Hospital" title="Central Hospital"/>
                </div>


                {/*<SearchBox styleName="d-none d-lg-block" placeholder=""*/}
                {/*           onChange={updateSearchText}*/}
                {/*           value={searchText}/>*/}

                <ul className="header-notifications list-inline ml-auto">
                    {/*<li className="list-inline-item">*/}
                    {/*  <Dropdown*/}
                    {/*    className="quick-menu app-notification"*/}
                    {/*    isOpen={apps}*/}
                    {/*    toggle={onAppsSelect}>*/}

                    {/*    <DropdownToggle*/}
                    {/*      className="d-inline-block"*/}
                    {/*      tag="span"*/}
                    {/*      data-toggle="dropdown">*/}
                    {/*      <span className="app-notification-menu">*/}
                    {/*        <i className="zmdi zmdi-apps zmdi-hc-fw zmdi-hc-lg"/>*/}
                    {/*        <span>Apps</span>*/}
                    {/*      </span>*/}
                    {/*    </DropdownToggle>*/}

                    {/*    <DropdownMenu>*/}
                    {/*      {Apps()}*/}
                    {/*    </DropdownMenu>*/}
                    {/*  </Dropdown>*/}
                    {/*</li>*/}
                    {/*<li className="d-inline-block d-lg-none list-inline-item">*/}
                    {/*  <Dropdown*/}
                    {/*    className="quick-menu nav-searchbox"*/}
                    {/*    isOpen={searchBox}*/}
                    {/*    toggle={onSearchBoxSelect}>*/}

                    {/*    <DropdownToggle*/}
                    {/*      className="d-inline-block"*/}
                    {/*      tag="span"*/}
                    {/*      data-toggle="dropdown">*/}
                    {/*      <IconButton className="icon-btn">*/}
                    {/*        <i className="zmdi zmdi-search zmdi-hc-fw"/>*/}
                    {/*      </IconButton>*/}
                    {/*    </DropdownToggle>*/}

                    {/*    <DropdownMenu right className="p-0">*/}
                    {/*      <SearchBox styleName="search-dropdown" placeholder=""*/}
                    {/*                 onChange={updateSearchText}*/}
                    {/*                 value={searchText}/>*/}
                    {/*    </DropdownMenu>*/}
                    {/*  </Dropdown>*/}
                    {/*</li>*/}
                    {/*<li className="list-inline-item">*/}
                    {/*  <Dropdown*/}
                    {/*    className="quick-menu"*/}
                    {/*    isOpen={langSwitcher}*/}
                    {/*    toggle={onLangSwitcherSelect}>*/}

                    {/*    <DropdownToggle*/}
                    {/*      className="d-inline-block"*/}
                    {/*      tag="span"*/}
                    {/*      data-toggle="dropdown">*/}
                    {/*      <IconButton className="icon-btn">*/}
                    {/*        <i className={`flag flag-24 flag-${locale.icon}`}/>*/}
                    {/*      </IconButton>*/}
                    {/*    </DropdownToggle>*/}

                    {/*    <DropdownMenu right className="w-50">*/}
                    {/*      <LanguageSwitcher switchLanguage={onSwitchLanguage}*/}
                    {/*                        handleRequestClose={handleRequestClose}/>*/}
                    {/*    </DropdownMenu>*/}
                    {/*  </Dropdown>*/}


                    {/*</li>*/}
                    {/*<li className="list-inline-item app-tour">*/}
                    {/*  <Dropdown*/}
                    {/*    className="quick-menu"*/}
                    {/*    isOpen={appNotification}*/}
                    {/*    toggle={onAppNotificationSelect}>*/}

                    {/*    <DropdownToggle*/}
                    {/*      className="d-inline-block"*/}
                    {/*      tag="span"*/}
                    {/*      data-toggle="dropdown">*/}
                    {/*      <IconButton className="icon-btn">*/}
                    {/*        <i className="zmdi zmdi-notifications-none icon-alert animated infinite wobble"/>*/}
                    {/*      </IconButton>*/}
                    {/*    </DropdownToggle>*/}

                    {/*    <DropdownMenu right>*/}
                    {/*      <CardHeader styleName="align-items-center"*/}
                    {/*                  heading={<IntlMessages id="appNotification.title"/>}/>*/}
                    {/*      <AppNotification/>*/}
                    {/*    </DropdownMenu>*/}
                    {/*  </Dropdown>*/}
                    {/*</li>*/}
                    {/*<li className="list-inline-item mail-tour">*/}
                    {/*  <Dropdown*/}
                    {/*    className="quick-menu"*/}
                    {/*    isOpen={mailNotification}*/}
                    {/*    toggle={onMailNotificationSelect}*/}
                    {/*  >*/}
                    {/*    <DropdownToggle*/}
                    {/*      className="d-inline-block"*/}
                    {/*      tag="span"*/}
                    {/*      data-toggle="dropdown">*/}

                    {/*      <IconButton className="icon-btn">*/}
                    {/*        <i className="zmdi zmdi-comment-alt-text zmdi-hc-fw"/>*/}
                    {/*      </IconButton>*/}
                    {/*    </DropdownToggle>*/}


                    {/*    <DropdownMenu right>*/}
                    {/*      <CardHeader styleName="align-items-center"*/}
                    {/*                  heading={<IntlMessages id="mailNotification.title"/>}/>*/}
                    {/*      <MailNotification/>*/}
                    {/*    </DropdownMenu>*/}
                    {/*  </Dropdown>*/}
                    {/*</li>*/}

                    <li className="list-inline-item user-nav">
                        <Dropdown
                            className="quick-menu"
                            isOpen={userInfo}
                            toggle={onUserInfoSelect}>

                            <DropdownToggle
                                className="d-inline-block"
                                tag="span"
                                data-toggle="dropdown">
                                <IconButton className="icon-btn size-30">
                                    <Avatar
                                        alt='...'
                                        src="https://www.asiapropertyawards.com/wp-content/uploads/2020/03/default-user-avatar-thumbnail@2x-ad6390912469759cda3106088905fa5bfbadc41532fbaa28237209b1aa976fc9.png"
                                        className="size-30"
                                    />
                                </IconButton>
                            </DropdownToggle>

                            <DropdownMenu right>
                                <UserInfoPopup/>
                            </DropdownMenu>
                        </Dropdown>
                    </li>
                </ul>

                <div className="ellipse-shape"/>
            </Toolbar>
        </AppBar>
    );
};


export default withRouter(Header);
