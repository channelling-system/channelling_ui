import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux';
import Button from '@material-ui/core/Button';
import IntlMessages from 'util/IntlMessages';
import {userSignIn} from 'actions/Auth';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import {hideMessage} from "../actions";


const SignIn = (props) => {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const token = useSelector(({auth}) => auth.token);
  const {error} = useSelector(({commonData}) => commonData);


  useEffect(() => {
    if (error) {
      setTimeout(() => {
        dispatch(hideMessage());
      }, 100);
    }
    if (token !== null) {
      props.history.push('/');
    }
  }, [token, error]);

  return (
      <div
          className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="login-content">
          <div className="login-header mt-2 d-flex justify-content-center">
            <div className="app-logo" title="Logo">
              {/*<h2 className="jr-font-weight-bold font-italic text-primary">*/}
              {/*  <IntlMessages id="app.name"/>*/}
              {/*</h2>*/}
              <img src={require("assets/images/logo.png")} alt="logo" title="logo"/>
            </div>
          </div>

          <div className="login-form mt-4">
            <form>
              <fieldset>
                <div className="form-group">
                  <input name="username" id="username" className="form-control form-control-lg"
                         placeholder="Username" type="text"
                         onChange={(event) => setUsername(event.target.value)}/>
                </div>
                <div className="form-group">
                  <input name="password" id="password" className="form-control form-control-lg"
                         placeholder="Password" type="password"
                         onChange={(event) => setPassword(event.target.value)}/>
                </div>
                <div className="form-group mt-4 d-flex justify-content-between align-items-center">
                  <label className="custom-control custom-checkbox float-left pl-0">
                    <input type="checkbox" className="custom-control-input"/>
                    <span className="custom-control-indicator"/>
                    <span className="custom-control-description">Remember me</span>
                  </label>

                  <div>
                    <Link to="/app/app-module/forgot-password-1"
                          title="Reset Password"><IntlMessages id="appModule.forgotPassword"/></Link>
                  </div>
                </div>

                <Button className="bg-primary py-2 text-white mt-3" fullWidth={true}
                        onClick={() => {dispatch(userSignIn({username, password}));}}>
                  <IntlMessages id="appModule.signIn"/>
                </Button>
                <div className="d-flex justify-content-center mt-3">
                  <Link to="/signup">
                    <IntlMessages id="signIn.signUp"/>
                  </Link>
                </div>

              </fieldset>
            </form>
          </div>
        </div>
        {error?NotificationManager.error(error):null}
        <NotificationContainer/>
      </div>
  );
};


export default SignIn;
