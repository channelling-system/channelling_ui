import {INIT_URL, SESSION_DATA} from "../constants/ActionTypes";

const INIT_STATE = {

};

export default (state = INIT_STATE, action) => {
  switch (action.type) {


    case INIT_URL: {
      return {...state, initURL: action.payload};
    }


    case SESSION_DATA: {
      return {
        ...state,
        sessions: action.payload,
      };
    }

    default:
      return state;
  }
}
