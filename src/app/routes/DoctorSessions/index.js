import React from 'react';
import Button from "@material-ui/core/Button";
import SessionTable from "./SessionTable";
import {NotificationContainer} from "react-notifications";
import SessionPopup from "./SessionPopup";
import axios from "../../../util/Api";
import {NotificationManager} from 'react-notifications';


class DoctorSessions extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            sessions: [],
            date: new Date(),
            lotInfoOpen: false,
        }
    }

    componentDidMount() {
        this.getAllSessions();
    }

    getAllSessions() {
        console.log('get sessions');
        axios.get('sessions',
        ).then(({data}) => {
            if (data) {
                this.setState({sessions: data})
            } else {
                // dispatch({type: FETCH_ERROR, payload: data.error});
            }
        }).catch(function (error) {
            // dispatch({type: FETCH_ERROR, payload: error.message});
            console.log("Error****:", error.message);
        });
    }

    handleDateChange(event) {
        this.setState({date: event.target.value})
    }


    openLotInfoPopup = () => {
        this.setState({open: false, lotInfoOpen: true});
    };
    closeLotInfoPopup = () => {
        this.setState({lotInfoOpen: false});
    };

    saveSession(data) {
        axios.post('sessions', data
        ).then(({data}) => {
            this.getAllSessions();
            this.setState({lotInfoOpen: false});
            NotificationManager.success('Session created successfully');
        }).catch(function (error) {
            NotificationManager.error('Failed to create session');
            console.log("Error****:", error.message);
        });
    }

    saveChanges(data) {
        this.saveSession(data);
    }


    render() {
        return (

            <div className="app-wrapper h-100">
                <div className="page-heading mb-3 py-3">
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">Doctor Sessions</h2>
                        <div className="ml-auto jr-btn-group">

                            <Button onClick={this.openLotInfoPopup} className="bg-primary px-3 ml-1">
                                Add Session
                            </Button>
                            <SessionPopup data={this.state.lotToBeEdited} open={this.state.lotInfoOpen}
                                          onClose={this.closeLotInfoPopup}
                                          onSave={(e) => this.saveChanges(e)} title="Add"/>

                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="jr-card jr-card-widget jr-card-full card px-3 py-3">
                            <SessionTable data={this.state.sessions} onDataChange={() => this.getAllSessions()}/>
                            <NotificationContainer/>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default DoctorSessions;
