import React, {Component} from 'react';
import moment from 'moment';
import {TimePicker, MuiPickersUtilsProvider, DatePicker} from '@material-ui/pickers';
import MomentUtils from "@date-io/moment";

export default class TimePickers extends Component {
    state = {
        selectedDate: moment(),
        time: null
    };

    onTimeChange = (date) => {
        this.props.onChange(moment(date).format('HH:mm'))
    }

    createDate(date, time) {
        date = new Date(date);
        time = time.split(":");
        date.setHours(time[0]);
        date.setMinutes(time[1]);
        return date;
    }

    render() {
        return (<div key="basic_time" className="picker mt-3">
            <MuiPickersUtilsProvider utils={MomentUtils}>
                <TimePicker
                    disabled={this.props.disabled}
                    label={this.props.label}
                    format="HH:mm"
                    fullWidth
                    value={this.createDate(this.props.date, this.props.time)}
                    onChange={this.onTimeChange}
                />
            </MuiPickersUtilsProvider>
        </div>)
    }
}
