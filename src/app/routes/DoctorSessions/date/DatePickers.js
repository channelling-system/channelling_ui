import React, {Component} from 'react';
import {DatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import MomentUtils from "@date-io/moment";
import moment from 'moment';

export default class DatePickers extends Component {


    onTimeChange = (date) => {
        date = moment(date).format('yyyy-MM-DD');
        this.props.onChange(date)
    }


    render() {
        return (
            <div key="basic_day" className={`picker ${this.props.noPadding?'':'mt-3'}`}>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <DatePicker
                        disabled={this.props.disabled}
                        label={this.props.label}
                        maxDate={this.props.maxDate}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={this.props.value}
                        error={this.props.error}
                        format="yyyy-MM-DD"
                        onChange={this.onTimeChange}
                        animateYearScrolling={false}
                        fullWidth={true}
                    />
                </MuiPickersUtilsProvider>
            </div>
        )
    }
};

