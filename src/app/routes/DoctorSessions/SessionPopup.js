import React from "react";

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from "@material-ui/core/CircularProgress";
// import {Button} from "reactstrap";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import DatePickers from "./date";
import TimePickers from "./time";
import Divider from "@material-ui/core/Divider";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "../../../util/Api";
import TextField from "@material-ui/core/TextField/TextField";
import {getAllDoctors} from "../../../actions/Doctor";



class SessionPopup extends React.Component {


    sessionStatuses = [
        {status: 'STARTED', label: 'Arrived'},
        {status: 'NOT_STARTED', label: 'Not Arrived'},
        {status: 'DELAYED', label: 'Delayed'},
        {status: 'COMPLETED', label: 'Completed'},
        {status: 'CANCELLED', label: 'Cancelled'},
        {status: 'HELD', label: 'Hold'}
    ]

    delays = [
        {label: '30 mins', value: '30'},
        {label: '1 hr', value: '60'},
        {label: '1 hr 30 mins', value: '90'},
        {label: '2 hrs', value: '120'},
    ]

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            doctors: [],
            rooms: []

        };
        this.onSaveClick = this.onSaveClick.bind(this);
    }

    componentDidMount() {
        this.getAllDoctors();
        this.getAllRooms();

        if (this.props.data) {
            let data = this.props.data;
            this.setState({
                id: data.id,
                doctorId: data.doctor.id,
                roomId: data.room.id,
                date: data.date,
                start: data.start,
                end: data.end,
                status: data.status,
                note: data.note,
                delayInMinutes: data.delayInMinutes
            })
        } else {
            this.setState({
                doctorId: 0,
                roomId: 0,
                id: 0,
                date: new Date(),
                start: '00:00',
                end: '00:00',
                status: 'NOT_STARTED',
                delayInMinutes: 0,
                note: ''
            })
        }
    }

    getAllDoctors() {
        getAllDoctors().then(({data}) => {
            this.setState({doctors: data})
        });
    }

    getAllRooms() {
        axios.get('rooms',
        ).then(({data}) => {
            if (data) {
                this.setState({rooms: data})
            }
        }).catch(function (error) {
            console.log("Error****:", error.message);
        });
    }

    onSaveClick() {
        this.props.onSave(
        {
            id: this.state.id,
            doctorId: this.state.doctorId,
            roomId: this.state.roomId,
            date: this.state.date,
            start: this.state.start,
            end: this.state.end,
            status: this.state.status,
            note: this.state.note,
            delayInMinutes: this.state.delayInMinutes
        }
        )
    }

    handleInputChange(event) {
        if (event.target.id) {
            this.setState({
                [event.target.id]: event.target.value
            });
        } else {
            this.setState({
                [event.target.name]: event.target.value
            });
        }
    }

    isDelayed() {
        return this.state.status === 'DELAYED';
    }


    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"xs"} open={this.props.open} onClose={this.props.onClose}>
                {this.state.loading ?
                    <div className="absolute-fit d-flex align-items-center justify-content-center bg-transparent-white">
                        {/*<div className="flex-grow-1">*/}
                        <CircularProgress/>
                        {/*</div>*/}
                    </div>
                    : null}
                <DialogTitle>
                    {this.props.title} Doctor Session
                </DialogTitle>
                <Divider className="" light/>
                <DialogContent>

                    <form>
                        <div className="row">
                            <div className={`col-12 ${this.isDelayed()?'col-lg-4':'col-lg-6'}`}>
                                <label className="text-light-grey font-size-12 mt-3">Doctor</label>
                                <Select
                                    id="doctorId"
                                    label={"Doctor"}
                                    name="doctorId"
                                    value={this.state.doctorId}
                                    disabled={this.props.editMode}
                                    onChange={(event => this.handleInputChange(event))}
                                    // input={<Input id="age-simple" /> }
                                    fullWidth
                                >
                                    {this.state.doctors.map((d, index) => {
                                        return (
                                            <MenuItem key={index} value={d.id}>{d.name}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </div>
                            <div className={`col-12 ${this.isDelayed()?'col-lg-4':'col-lg-6'}`}>
                                <label className="text-light-grey font-size-12 mt-3">Status</label>
                                <Select
                                    id="status"
                                    label={"Status"}
                                    name="status"
                                    value={this.state.status}
                                    onChange={(event => this.handleInputChange(event))}
                                    // input={<Input id="age-simple" /> }
                                    fullWidth
                                >
                                    {this.sessionStatuses.map((s, index) => {
                                        return (
                                            <MenuItem key={index} value={s.status}>{s.label}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </div>
                            {this.isDelayed()?
                                <div className={`col-12 col-lg-4`}>
                                    <label className="text-light-grey font-size-12 mt-3">Delayed</label>
                                    <Select
                                        name="delayInMinutes"
                                        value={this.state.delayInMinutes}
                                        onChange={(event) => this.handleInputChange(event)}
                                        // input={<Input id="age-simple" /> }
                                        fullWidth
                                    >
                                        {this.delays.map((d, index) => {
                                            return (
                                                <MenuItem key={index} value={d.value}>{d.label}</MenuItem>
                                            )
                                        })}
                                    </Select>
                                </div>
                            :null}
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-4 mt-1">
                                <DatePickers disabled={this.props.editMode} label="Date" value={this.state.date} onChange={(event) => {
                                    this.setState({date: event})
                                }}/>
                            </div>
                            <div className="col-12 col-lg-4 mt-1">
                                <TimePickers date={this.state.date} time={this.state.start}
                                             disabled={this.props.editMode}
                                             onChange={(event) => {
                                                 this.setState({start: event})
                                             }}
                                             label="Starting Time"/>
                            </div>
                            <div className="col-12 col-lg-4 mt-1">
                                <TimePickers date={this.state.date} time={this.state.end}
                                             disabled={this.props.editMode}
                                             onChange={(event) => {
                                                 this.setState({end: event})
                                             }}
                                             label="Ending Time"/>
                            </div>
                        </div>

                        <div className="row mt-3">
                            <div className="col-12 col-lg-6">
                                <label className="text-light-grey font-size-12 mt-1">Room</label>
                                <Select
                                    id="roomId"
                                    label={"Room"}
                                    name="roomId"
                                    value={this.state.roomId}
                                    onChange={(event => this.handleInputChange(event))}
                                    // input={<Input id="age-simple" /> }
                                    fullWidth
                                >
                                    {this.state.rooms.map((r, index) => {
                                        return (
                                            <MenuItem key={index} value={r.id}>{r.name}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </div>
                        </div>
                    </form>
                </DialogContent>
                {/*<Divider className="" light/>*/}
                <DialogActions className="mx-3 mb-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                    <Button onClick={this.onSaveClick} className="bg-primary px-3 ml-1">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }


};

export default SessionPopup;
