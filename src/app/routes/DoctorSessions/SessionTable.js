import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import SessionPopup from "./SessionPopup";
import axios from "../../../util/Api";
import {NotificationManager} from 'react-notifications';


class SessionTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: undefined,
            open: false,
            lotInfoOpen: false,
            items: this.props.data,
            sessionToBeEdited: null
        };
        // this.items = this.props.data;
        this.saveChanges = this.saveChanges.bind(this)

    }


    handleClick = (event, n) => {
        this.setState({open: true, anchorEl: event.currentTarget, sessionToBeEdited: n});
    };

    handleRequestClose = () => {
        this.setState({open: false});
    };

    openLotInfoPopup = () => {
        this.setState({open: false, lotInfoOpen: true});
    };

    closeLotInfoPopup = () => {
        this.setState({lotInfoOpen: false});
    };

    render() {
        return (
            <div className="table-container table-responsive-material">
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell className="table-header-cell-sticky" align="left">Doctor Name</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Speciality</TableCell>
                            <TableCell className="table-header-cell-sticky" align="center">Date</TableCell>
                            <TableCell className="table-header-cell-sticky" align="center">Starting Time</TableCell>
                            <TableCell className="table-header-cell-sticky" align="center">Ending Time</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Room No</TableCell>
                            {/*<TableCell className="table-header-cell-sticky" align="left">No. of Patients</TableCell>*/}
                            <TableCell className="table-header-cell-sticky" align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>

                        {this.props.data.map((data, index) => (
                            <TableRow key={index}>
                                <TableCell>{data.doctor.name}</TableCell>
                                <TableCell align="left">{data.doctor.speciality}</TableCell>
                                <TableCell align="center">{data.date}</TableCell>
                                <TableCell align="center">{data.start.substring(0, data.start.length -3)}</TableCell>
                                <TableCell align="center">{data.end.substring(0, data.end.length -3)}</TableCell>
                                <TableCell align="left">{data.room.name}</TableCell>
                                {/*<TableCell align="left">{data.patients}</TableCell>*/}
                                <TableCell align="center">
                                    <IconButton className="text-light icon-btn"
                                                onClick={(event) => this.handleClick(event, data)}
                                    >
                                        <i className="zmdi zmdi-more-vert text-grey"/>
                                    </IconButton>
                                    <Menu
                                        elevation={1}
                                        position={`left`}
                                        anchorEl={this.state.anchorEl}
                                        open={this.state.open}
                                        onClose={this.handleRequestClose}
                                    >
                                        <MenuItem onClick={this.openLotInfoPopup}>Edit</MenuItem>
                                    </Menu>
                                </TableCell>
                            </TableRow>
                        ))}


                    </TableBody>
                </Table>
                {this.state.lotInfoOpen ?
                    <SessionPopup
                        data={this.state.sessionToBeEdited}
                        open={this.state.lotInfoOpen}
                        editMode={true}
                        onClose={this.closeLotInfoPopup}
                        onSave={(e) => this.saveChanges(e)} title="Edit"/>
                    : null}
            </div>
        );
    }

    saveChanges(e) {
        this.updateSession(e.id, e);
    }

    updateSession(id, data) {
        axios.put('sessions/' + id, data
        ).then(({data}) => {
           this.props.onDataChange();
            this.setState({lotInfoOpen: false});
            NotificationManager.success('Session updated successfully')
        }).catch(function (error) {
            NotificationManager.error('Session update failed');
            console.log("Error****:", error.message);
        });
    }


}


export default SessionTable;
