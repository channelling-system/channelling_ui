import React from 'react';
import Button from "@material-ui/core/Button";
import RoomTable from "./RoomTable";
import {NotificationContainer, NotificationManager} from "react-notifications";
import RoomPopup from "./RoomPopup";
import axios from "../../../util/Api";


class Rooms extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            rooms: [],
            date: new Date(),
            lotInfoOpen: false,
        }
    }

    componentDidMount() {
        this.getAllRooms();
    }

    getAllRooms() {
        axios.get('rooms',
        ).then(({data}) => {
            if (data) {
                this.setState({rooms: data})
            }
        }).catch(function (error) {
            console.log("Error****:", error.message);
        });
    }

    handleDateChange(event) {
        this.setState({date: event.target.value})
    }


    openLotInfoPopup = () => {
        this.setState({open: false, lotInfoOpen: true});
    };
    closeLotInfoPopup = () => {
        this.setState({lotInfoOpen: false});
    };

    saveRoom(data) {
        axios.post('rooms', data
        ).then(({data}) => {
            this.getAllRooms();
            this.setState({lotInfoOpen: false});
            NotificationManager.success('Room created successfully');
        }).catch(function (error) {
            NotificationManager.error('Failed to create room');
            console.log("Error****:", error.message);
        });
    }

    saveChanges(data) {
        this.saveRoom(data);
    }


    render() {
        return (

            <div className="app-wrapper h-100">
                <div className="page-heading mb-3 py-3">
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">Rooms</h2>
                        <div className="ml-auto jr-btn-group">

                            <Button onClick={this.openLotInfoPopup} className="bg-primary px-3">
                                Add Room
                            </Button>
                            <RoomPopup data={this.state.lotToBeEdited} open={this.state.lotInfoOpen}
                                          onClose={this.closeLotInfoPopup}
                                          onSave={(e) => this.saveChanges(e)} title="Add"/>

                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="jr-card jr-card-widget jr-card-full card px-3 py-3">
                            <RoomTable data={this.state.rooms} onDataChange={() => this.getAllRooms()}/>
                            <NotificationContainer/>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Rooms;
