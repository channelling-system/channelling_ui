import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import RoomPopup from "./RoomPopup";
import axios from "../../../util/Api";
import {NotificationManager} from 'react-notifications';


class RoomTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: undefined,
            open: false,
            roomInfoOpen: false,
            items: this.props.data,
            roomToBeEdited: null
        };
        // this.items = this.props.data;
        this.saveChanges = this.saveChanges.bind(this)

    }


    handleClick = (event, n) => {
        this.setState({open: true, anchorEl: event.currentTarget, roomToBeEdited: n});
    };

    handleRequestClose = () => {
        this.setState({open: false});
    };

    openRoomInfoPopup = () => {
        this.setState({open: false, roomInfoOpen: true});
    };

    closeRoomInfoPopup = () => {
        this.setState({roomInfoOpen: false});
    };

    render() {
        return (
            <div className="table-container table-responsive-material">
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell className="table-header-cell-sticky" align="left">Room Name</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Description</TableCell>
                            <TableCell className="table-header-cell-sticky" align="right">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>

                        {this.props.data.map((data, index) => (
                            <TableRow key={index}>
                                <TableCell>{data.name}</TableCell>
                                <TableCell align="left">{data.description}</TableCell>
                                <TableCell align="right">
                                    <IconButton className="text-light icon-btn"
                                                onClick={(event) => this.handleClick(event, data)}
                                    >
                                        <i className="zmdi zmdi-more-vert text-grey"/>
                                    </IconButton>
                                    <Menu
                                        elevation={1}
                                        position={`left`}
                                        anchorEl={this.state.anchorEl}
                                        open={this.state.open}
                                        onClose={this.handleRequestClose}
                                    >
                                        <MenuItem onClick={this.openRoomInfoPopup}>Edit</MenuItem>
                                    </Menu>
                                </TableCell>
                            </TableRow>
                        ))}


                    </TableBody>
                </Table>
                {this.state.roomInfoOpen ?
                    <RoomPopup
                        data={this.state.roomToBeEdited}
                        open={this.state.roomInfoOpen}
                        onClose={this.closeRoomInfoPopup}
                        onSave={(e) => this.saveChanges(e)} title="Edit"/>
                    : null}
            </div>
        );
    }

    saveChanges(e) {
        this.updateSession(e.id, e);
    }

    updateSession(id, data) {
        axios.put('rooms/' + id, data
        ).then(({data}) => {
           this.props.onDataChange();
            this.setState({roomInfoOpen: false});
            NotificationManager.success('Room updated successfully')
        }).catch(function (error) {
            NotificationManager.error('Room update failed');
            console.log("Error****:", error.message);
        });
    }


}


export default RoomTable;
