import React from "react";

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from "@material-ui/core/CircularProgress";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import FormHelperText from "@material-ui/core/FormHelperText";


class RoomPopup extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            id: 0,
            name: '',
            description: '',
            errors: {}

        };
        this.onSaveClick = this.onSaveClick.bind(this);
    }

    componentDidMount() {

        if (this.props.data) {
            let data = this.props.data;
            this.setState({
                id: data.id,
                name: data.name,
                description: data.description
            })
        }
    }



    onSaveClick() {
        if (this.isFormValid()) {
            this.props.onSave(
                {
                    id: this.state.id,
                    name: this.state.name,
                    description: this.state.description,
                }
            )
        }
    }

    handleInputChange(event) {
        if (event.target.id) {
            this.setState({
                [event.target.id]: event.target.value
            });
        } else {
            this.setState({
                [event.target.name]: event.target.value
            });
        }
    }

    isFormValid() {

        const errors = {};

        if (!this.state.name) {
            errors.name = "Required";
        }

        this.setState({errors: errors});

        return !Object.keys(errors).length;
    };


    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"xs"} open={this.props.open} onClose={this.props.onClose}>
                {this.state.loading ?
                    <div className="absolute-fit d-flex align-items-center justify-content-center bg-transparent-white">
                        <CircularProgress/>
                    </div>
                    : null}
                <DialogTitle>
                    {this.props.title} Room
                </DialogTitle>
                <Divider className="" light/>
                <DialogContent>

                    <form>
                        <div className="row">
                            <div className={`col-12`}>
                                <TextField
                                    id="name"
                                    label={"Name"}
                                    value={this.state.name}
                                    error={this.state.errors && this.state.errors.name}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                                {this.state.errors ?
                                    <FormHelperText className="text-red">{this.state.errors.name}</FormHelperText>
                                    : null
                                }
                            </div>
                            <div className={`col-12`}>
                                <TextField
                                    id="description"
                                    label={"Description"}
                                    value={this.state.description}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                    multiline
                                />
                            </div>
                        </div>
                    </form>
                </DialogContent>
                <DialogActions className="mx-3 mb-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                    <Button onClick={this.onSaveClick} className="bg-primary px-3 ml-1">
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }


};

export default RoomPopup;
