import React from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import asyncComponent from '../../util/asyncComponent';

const Routes = ({match}) =>

    <Switch>
        <Route path={`${match.url}/dashboard`}
               component={asyncComponent(() => import('./Dashboard'))}/>
        <Route path={`${match.url}/doctors`}
               component={asyncComponent(() => import('./Doctor'))}/>
        <Route path={`${match.url}/sessions`}
               component={asyncComponent(() => import('./DoctorSessions'))}/>
        <Route path={`${match.url}/rooms`}
               component={asyncComponent(() => import('./Rooms'))}/>
        <Route path={`${match.url}/appointments`}
               component={asyncComponent(() => import('./Appointments'))}/>
        {/*<Route component={asyncComponent(() => import("app/routes/extraPages/routes/404"))}/>*/}
    </Switch>;


export default withRouter(Routes);

