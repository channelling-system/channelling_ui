import React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import TextField from "@material-ui/core/TextField/TextField";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Divider from "@material-ui/core/Divider";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";

class DoctorDetails extends React.Component {

    specialities = [
        { displayName: 'Physician', value: 'PHYSICIAN' }, { displayName: 'Cardiologist', value: 'CARDIOLOGIST' },
        { displayName: 'Consultant obstetrician and gynaecologist', value: 'CONSULTANT_OBSTETRICIAN_AND_GYNAECOLOGIST' },
        { displayName: 'Radiologist', value: 'RADIOLOGIST' },
        { displayName: 'Ear, Nose and Throat (ENT) Surgeon', value: 'EAR_NOSE_AND_THROAT_ENT_SURGEON' },
        { displayName: 'Ophthalmologist', value: 'OPHTHALMOLOGIST' },
        { displayName: 'Dermatologist', value: 'DERMATOLOGIST' }, { displayName: 'Chest Physician', value: 'CHEST_PHYSICIAN' },
        { displayName: 'Pediatrician', value: 'PEDIATRICIAN' },
        { displayName: 'Consultant Neuro Physiologist', value: 'CONSULTANT_NEURO_PHYSIOLOGIST' },
        { displayName: 'General Surgeon', value: 'GENERAL_SURGEON' },
        { displayName: 'Psychiatrist', value: 'PSYCHIATRIST' },
        { displayName: 'Endocrinologist', value: 'ENDOCRINOLOGIST' },
        { displayName: 'OPD', value: 'OPD' },
        { displayName: 'Diabetes Clinic', value: 'DIABETES_CLINIC' }
    ];

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            id: "",
            name: "",
            speciality: "",
            telephone: "",
            email: "",
            fee: "",
            consultationTime: ""
        };
        this.onSaveClick = this.onSaveClick.bind(this);
    }

    componentDidMount() {
        if (this.props.data) {
            for (const [key, value] of Object.entries(this.props.data)) {
                // console.log(`There are ${count} ${fruit}s`)
                this.setState({
                    [key]: value
                });
            }
        }
    }

    handleInputChange(event) {
        let val = event.target.value;
        if (event.target.id === 'telephone' && val.length > 10) {
            val = val.substring(0, 10)
        }
        this.setState({
            [event.target.id]: val
        });
    }

    handleSelectSpeciality(event) {
        this.setState({ speciality: event.target.value })
    }

    onSaveClick() {
        if (this.isFormValid()) {
            this.props.onSave(
                {
                    id: this.state.id,
                    name: this.state.name,
                    speciality: this.state.speciality,
                    telephone: this.state.telephone,
                    email: this.state.email,
                    fee: this.state.fee,
                    consultationTime: this.state.consultationTime
                }
            );
            this.setState({
                id: "",
                name: "",
                speciality: "",
                telephone: "",
                email: "",
                fee: "",
                consultationTime: ""
            });
        }

    }

    isTelephoneNumberValid(val) {
        return /^\d{10}$/.test(val);
    }

    isFormValid() {

        const errors = {};

        if (!this.state.name) {
            errors.name = "Required";
        }
        if (!this.state.speciality) {
            errors.speciality = "Required";
        }
        if (!this.state.telephone) {
            errors.telephone = "Required";
        } else if (!this.isTelephoneNumberValid(this.state.telephone)) {
            errors.telephone = "Invalid Number";
        }
        if (!this.state.fee || this.state.fee === 0) {
            errors.fee = "Required";
        }
        if (this.state.consultationTime && this.state.consultationTime > 30) {
            errors.consultationTime = "Maximum 30 mins";
        }
        this.setState({ errors: errors });

        return !Object.keys(errors).length;
    };


    render() {

        return (
            <Dialog disableBackdropClick maxWidth={"xl"} open={this.props.open} onClose={this.props.onClose}>
                <DialogTitle>
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">{this.props.title} Doctor</h2>
                        <div className="ml-auto jr-btn-group">

                        </div>

                    </div>
                </DialogTitle>
                <Divider className="" light />
                <DialogContent>
                    <form>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="name"
                                    label={"Doctor Name"}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    value={this.state.name}
                                    error={this.state.errors && this.state.errors.name}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                                {this.state.errors ?
                                    <FormHelperText className="text-red">{this.state.errors.name}</FormHelperText>
                                    : null
                                }
                            </div>
                            <div className="col-12 col-lg-6">
                                <label className="text-light-grey font-size-12 mt-3">Speciality</label>
                                <Select
                                    id="speciality"
                                    label={"Status"}
                                    name="status"
                                    value={this.state.speciality}
                                    onChange={(event => this.handleSelectSpeciality(event))}
                                    error={this.state.errors && this.state.errors.speciality}
                                    fullWidth
                                >
                                    {this.specialities.map((s, index) => {
                                        return (
                                            <MenuItem key={index} value={s.value}>{s.displayName}</MenuItem>
                                        )
                                    })}
                                </Select>
                                {this.state.errors ?
                                    <FormHelperText className="text-red">{this.state.errors.speciality}</FormHelperText>
                                    : null
                                }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="telephone"
                                    label={"Mobile No"}
                                    value={this.state.telephone}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    placeholder="07XXXXXXXX"
                                    error={this.state.errors && this.state.errors.telephone}
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                                {this.state.errors ?
                                    <FormHelperText className="text-red">{this.state.errors.telephone}</FormHelperText>
                                    : null
                                }
                            </div>
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="email"
                                    label={"Email"}
                                    value={this.state.email}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="consultationTime"
                                    label={"Average Time (Mins)"}
                                    value={this.state.consultationTime}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    error={this.state.errors && this.state.errors.consultationTime}
                                    margin="normal"
                                    type="number"
                                />
                                {this.state.errors ?
                                    <FormHelperText className="text-red">{this.state.errors.consultationTime}</FormHelperText>
                                    : null
                                }
                            </div>
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="fee"
                                    label={"Fee per Patient"}
                                    value={this.state.fee}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    error={this.state.errors && this.state.errors.fee}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="number"
                                />
                                {this.state.errors ?
                                    <FormHelperText className="text-red">{this.state.errors.fee}</FormHelperText>
                                    : null
                                }
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-12"><br></br>

                                {/*// below content here*/}

                            </div>

                        </div>

                    </form>
                </DialogContent>

                <DialogActions className="mx-3 mb-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                    <Button className="bg-primary ml-1 px-3" onClick={this.onSaveClick}>
                        {this.props.title}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default DoctorDetails;
