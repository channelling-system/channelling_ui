import React from "react";
import {NotificationContainer, NotificationManager} from "react-notifications";
import SessionTable from "./SessionTable";
import Button from "@material-ui/core/Button";
import DoctorDetails from "./DoctorDetails";
import {getAllDoctors, saveDoctor} from "../../../actions/Doctor";

class MyLots extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            open2: false,
            items: this.data,
            dataDoctor:[]
        }
        this.saveChanges = this.saveChanges.bind(this)
    }

    componentDidMount() {
        this.getAllDoctors();
    }

    getAllDoctors(){
        getAllDoctors().then(({data}) => {
            let dataDoctor = data;
            this.setState({dataDoctor: dataDoctor});
        })
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleRequestClose = () => {
        this.setState({open: false});
    };

    handleClickOpen2 = () => {
        this.setState({open2: true});
    };

    handleRequestClose2 = () => {
        this.setState({open2: false});
    };

    // saveChanges(e) {
    //     console.log(e)
    //     e.status = 'next';
    //     let items = this.state.items;
    //     items.push(e);
    //     this.setState({open: false, items: items});
    //
    // }


    render() {
        return (
            <div className="app-wrapper h-100">
                <div className="page-heading mb-3 py-3">
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">Manage Doctor</h2>
                        <div className="ml-auto jr-btn-group">
                            <Button onClick={this.handleClickOpen2} className="bg-primary px-3 ml-1">
                                Add Doctor
                            </Button>

                            <DoctorDetails open={this.state.open2} onClose={this.handleRequestClose2}
                                           onSave={(e) => this.saveChanges(e)} title="Add"/>
                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="jr-card jr-card-widget jr-card-full card px-3 py-3">
                            <SessionTable data={this.state.dataDoctor} onDataChange={() => this.getAllDoctors()}/>
                            <NotificationContainer/>
                        </div>
                    </div>
                </div>

            </div>

        );
    }

    saveChanges(e) {
        saveDoctor(e).then(({data}) => {
            this.getAllDoctors();
            // this.props.onDataChange();
            this.setState({open2: false});
            NotificationManager.success('Doctor added successfully')
        }).catch(function (error) {
            NotificationManager.error('Doctor addition failed');
            console.log("Error****:", error.message);
        });

    }
}

export default MyLots;
