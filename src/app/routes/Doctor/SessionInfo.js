import React from "react";

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from "@material-ui/core/CircularProgress";
import TextField from "@material-ui/core/TextField";
// import {Button} from "reactstrap";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import LotsTable from ".//LotsTable";
import {NotificationContainer} from "react-notifications";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {Badge} from "reactstrap";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import LotInfoPopup from ".//LotInfoPopup";
import Divider from "@material-ui/core/Divider";


class SessionInfo extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            loading: false,

        };
        this.onSaveClick = this.onSaveClick.bind(this);
    }

    componentDidMount() {
        if (this.props.data) {
            console.log(this.props.data)
            for (const [key, value] of Object.entries(this.props.data)) {
                // console.log(`There are ${count} ${fruit}s`)
                this.setState({
                    [key]: value
                });
            }
            // this.setState({
            //     form:
            //         {
            //             lotNo: this.props.data.lotNumber,
            //             grade: this.props.data.grade,
            //             noOfBags: this.props.data.noOfBags,
            //             sellingMark: this.props.data.sellingMark,
            //             invoice: this.props.data.invoice,
            //             weightPerBag: this.props.data.weightPerBag,
            //             netWeight: this.props.data.netWeight,
            //         },
            //
            // });

        }
    }

    onSaveClick() {
        console.log(this.state)
        this.props.onSave(this.state)
    }

    handleInputChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }


    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"xl"} open={this.props.open} onClose={this.props.onClose}>
                {this.state.loading ?
                    <div className="absolute-fit d-flex align-items-center justify-content-center bg-transparent-white">
                        {/*<div className="flex-grow-1">*/}
                        <CircularProgress/>
                        {/*</div>*/}
                    </div>
                    : null}
                <DialogTitle>
                    {/*<div className="d-flex flex-row">*/}
                    {/*    <span className="my-auto">{this.props.title}</span>*/}
                    {/*    <IconButton className="ml-auto my-auto" onClick={this.props.onClose}>*/}
                    {/*        <i className="zmdi zmdi-close text-grey size-20 jr-fs-xxl"/>*/}
                    {/*    </IconButton>*/}
                    {/*    */}
                    {/*</div>*/}
                    {this.props.title}
                </DialogTitle>
                <Divider className="" light/>
                <DialogContent>
                    <form>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="lotNumber"
                                    label={"Doctor Name"}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    // value={this.state.lotNumber}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="grade"
                                    label={"Speciallity"}
                                    // value={this.state.grade}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="brokerCode"
                                    label={"Date"}
                                    // value={this.state.brokerCode}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="invoice"
                                    label={"Time"}
                                    // value={this.state.invoice}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="sellingMark"
                                    label={"Room#"}
                                    // value={this.state.sellingMark}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                            <div className="col-12 col-lg-6">
                                <TextField
                                    id="startingBid"
                                    label={"No Of Patients"}
                                    // value={this.state.startingBid}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    fullWidth
                                    onChange={(event) => this.handleInputChange(event)}
                                    margin="normal"
                                    type="text"
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12 col-lg-12"><br></br>

                                <div className="table-container table-responsive-material">
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell className="table-header-cell-sticky">First Name</TableCell>
                                                <TableCell className="table-header-cell-sticky" align="right">Last
                                                    Name</TableCell>
                                                <TableCell className="table-header-cell-sticky"
                                                           align="right">NIC</TableCell>
                                                <TableCell className="table-header-cell-sticky"
                                                           align="right">DOB</TableCell>
                                                <TableCell className="table-header-cell-sticky"
                                                           align="right">Age</TableCell>
                                                <TableCell className="table-header-cell-sticky"
                                                           align="right">Address</TableCell>
                                                <TableCell className="table-header-cell-sticky"
                                                           align="right">Gender</TableCell>
                                                <TableCell className="table-header-cell-sticky"
                                                           align="center"></TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                <TableCell align="right">Amal</TableCell>
                                                <TableCell align="right">Perera</TableCell>
                                                <TableCell align="right">751051713V</TableCell>
                                                <TableCell align="right">1975-09-09</TableCell>
                                                <TableCell align="right">46</TableCell>
                                                <TableCell align="right">Matara</TableCell>
                                                <TableCell align="right">Male</TableCell>
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </div>

                            </div>

                        </div>

                    </form>
                </DialogContent>
                <DialogActions className="mx-3 mb-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }


};

export default SessionInfo;
