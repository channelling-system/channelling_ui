import React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Divider from "@material-ui/core/Divider";
import ReactToPrint from "react-to-print";
import ComponentToPrint from "./ComponentToPrint";
import PrintView from "./PrintView";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";

class PaymentDetails extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            action: -1,
            showInfo:false

        };
        this.onPrintClick = this.onPrintClick.bind(this);
    }


    onPrintClick(){
        // document.getElementById('test').innerHTML.
    }

    handleActionChange = (action) => {
        this.setState({action: action})
    }


    handleClickOpen2 = () => {
        this.setState({open2: true});
    };

    handleRequestClose2 = () => {
        this.setState({open2: false});
    };


    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"xl"} open={this.props.open} onClose={this.props.onClose}>
                <DialogTitle>
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">Appointment Details</h2>
                        <div className="ml-auto jr-btn-group">
                            {/*<button*/}
                            {/*    className="MuiButtonBase-root MuiButton-root MuiButton-contained jr-btn jr-btn-sm*/}
                            {/*MuiButton-containedPrimary mb-n2"*/}
                            {/*    tabIndex="0" type="button" onClick={this.handleClickOpen2}>*/}
                            {/*    <span className="MuiButton-label">Print</span>*/}
                            {/*    <span className="MuiTouchRipple-root"></span>*/}
                            {/*</button>*/}

                            <ReactToPrint
                                trigger={() => <Button href="#" className="bg-primary px-3">
                                    Print
                                </Button>}
                                content={() => this.componentRef}
                            />

                        </div>

                    </div>
                </DialogTitle>
                <Divider className="" light/>

                <div>
                    <ComponentToPrint data={this.props.data} onSave={this.props.onSave} onActionChange={this.handleActionChange} ref={el => (this.componentRef = el)} />

                    <div style={{ display: this.state.showInfo ? "block" : "none" }}>
                        <PrintView data={this.props.data} ref={el => (this.componentRef = el)} />
                    </div>


                </div>


                <Divider className="" light/>
                <DialogActions className="mx-3 mb-2 mt-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                    <Button disabled={this.state.action === -1} onClick={() => this.props.onSave(this.state.action)} className={`px-3 ${this.state.action === -1 ? 'bg-light': 'bg-primary'}`}>
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default PaymentDetails;
