import React from "react";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";

import "./PaymentDetails.css";

class PrintView extends React.Component {

    componentDidMount() {
        console.log(this.props.data);
    }

    render() {
        return (
            <DialogContent>

                <div className="ticket">
                    {/*<img src="./logo.png" alt="Logo">*/}
                    <p className="centered">KANDY CENTRAL HOSPITAL
                        <br></br>PRIVATE LIMITED
                        <br></br>
                        <mark id="title2">Specialist Appointment Confirmation</mark>
                    </p>

                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Doctor</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.session.doctor.name}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Speciality</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.session.doctor.speciality}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Time</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.estimatedTime}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Room</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.session.room.name}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Date</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.session.date}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Patient</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.patient.name}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Ref. No</div>
                        <div className="col-lg-6 col-md-6 col-6 price">{this.props.data.number}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Doctor Fee</div>
                        <div className="col-lg-6 col-md-6 col-6 price" align={"right"}>{this.props.data.doctorFee}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity">Hospital Fee</div>
                        <div className="col-lg-6 col-md-6 col-6 price" align={"right"}>{this.props.data.hospitalFee}</div>
                    </div>
                    <div className="row topboarder">
                        <div className="col-lg-6 col-md-6 col-6 quantity boldFont">Total</div>
                        <div className="col-lg-6 col-md-6 col-6 price boldFont" align={"right"}>{this.props.data.hospitalFee + this.props.data.doctorFee}</div>
                    </div>

                    {/*<table>*/}
                    {/*    /!*<thead>*!/*/}
                    {/*    /!*<tr>*!/*/}
                    {/*    /!*    <th className="quantity">Q.</th>*!/*/}
                    {/*    /!*    <th className="description">Description</th>*!/*/}
                    {/*    /!*    <th className="price">$$</th>*!/*/}
                    {/*    /!*</tr>*!/*/}
                    {/*    /!*</thead>*!/*/}
                    {/*    <tbody>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Doctor</td>*/}
                    {/*        <td className="price">{this.props.data.session.doctor.name}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Speciality</td>*/}
                    {/*        <td className="price">{this.props.data.session.doctor.speciality}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Time</td>*/}
                    {/*        <td className="price">{this.props.data.estimatedTime}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Room</td>*/}
                    {/*        <td className="price">{this.props.data.session.room.name}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Date</td>*/}
                    {/*        <td className="price">{this.props.data.session.date}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Patient</td>*/}
                    {/*        <td className="price">{this.props.data.patient.name}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Reference No</td>*/}
                    {/*        <td className="price">{this.props.data.number}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Doctor fee</td>*/}
                    {/*        <td className="price"  align={"right"}>{this.props.data.doctorFee}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr>*/}
                    {/*        <td className="quantity">Hospital fee</td>*/}
                    {/*        <td className="price"  align={"right"}>{this.props.data.hospitalFee}</td>*/}
                    {/*    </tr>*/}
                    {/*    <tr className={"totalFee"}>*/}
                    {/*        <td >Total</td>*/}
                    {/*        <td  align={"right"}>{this.props.data.hospitalFee + this.props.data.doctorFee}</td>*/}
                    {/*    </tr>*/}
                    {/*    </tbody>*/}
                    {/*</table>*/}
                    <p className="centered">Thank You!
                        <br></br></p>
                </div>


            </DialogContent>

        );
    }
}

export default PrintView;
