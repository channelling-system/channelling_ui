import React from "react";
import IconButton from "@material-ui/core/IconButton";
import Fab from "@material-ui/core/Fab";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import SessionInfo from "./SessionInfo";
import MakeAppointment from "./MakeAppointment";
import axios from 'util/Api'
import { getSessions } from "../../../actions/Sessions";
import { connect } from "react-redux";
import { Badge, Button } from "reactstrap";
import { NotificationContainer } from "react-notifications";
import Avatar from "@material-ui/core/Avatar";
import { getAllDoctors } from "../../../actions/Doctor";

class Dashboard extends React.Component {
    

    _isMounted = false;

    specialities = [
        { displayName: 'Physician', value: 'PHYSICIAN' }, { displayName: 'Cardiologist', value: 'CARDIOLOGIST' },
        { displayName: 'Consultant obstetrician and gynaecologist', value: 'CONSULTANT_OBSTETRICIAN_AND_GYNAECOLOGIST' },
        { displayName: 'Radiologist', value: 'RADIOLOGIST' },
        { displayName: 'Ear, Nose and Throat (ENT) Surgeon', value: 'EAR_NOSE_AND_THROAT_ENT_SURGEON' },
        { displayName: 'Ophthalmologist', value: 'OPHTHALMOLOGIST' },
        { displayName: 'Dermatologist', value: 'DERMATOLOGIST' }, { displayName: 'Chest Physician', value: 'CHEST_PHYSICIAN' },
        { displayName: 'Pediatrician', value: 'PEDIATRICIAN' },
        { displayName: 'Consultant Neuro Physiologist', value: 'CONSULTANT_NEURO_PHYSIOLOGIST' },
        { displayName: 'General Surgeon', value: 'GENERAL_SURGEON' },
        { displayName: 'Psychiatrist', value: 'PSYCHIATRIST' },
        { displayName: 'Endocrinologist', value: 'ENDOCRINOLOGIST' },
        { displayName: 'OPD', value: 'OPD' },
        { displayName: 'Diabetes Clinic', value: 'DIABETES_CLINIC' }
    ]

    statusColors = [
        { status: 'STARTED', color: 'green', label: 'Arrived' },
        { status: 'NOT_STARTED', color: 'orange', label: 'Not Arrived' },
        { status: 'DELAYED', color: 'amber', label: 'Delayed' },
        { status: 'COMPLETED', color: 'green', label: 'Completed' },
        { status: 'CANCELLED', color: 'red', label: 'Cancelled' },
        { status: 'HELD', color: 'blue', label: 'Hold' },
    ]

    searchDates = [
        { label: 'Today' },
        { label: 'Tomorrow' },
        { label: 'Next Week' }
    ]

    constructor(props) {
        super(props);
        this.state = {
            sessions: [],
            filteredSessions: [],
            searchName: -1,
            searchSpeciality: -1,
            searchDate: '',
            openedSession: null,
            open: false,
            open2: false,
            doctors: []
        }
        // this.loadSessions();
    }

    isAPastDate = (date) => {
        let today = new Date();
        today.setHours(0, 0, 0, 0);

        return date.setHours(0, 0, 0, 0) - today < 0;
    }

    loadSessions() {
        axios.get('sessions',
        ).then((r) => {
            let data = r.data.filter(d => !this.isAPastDate(new Date(d.date)));
            this.setState({ sessions: data, filteredSessions: data })
        }).catch(function (error) {
            console.log("Error****:", error.message);
        });
    }

    filterSessions2(date) {
        let sessions = this.state.sessions;
        let filterDate = new Date();
        let currentDate = new Date();
        let lastDate = null;

        switch (date) {
            case 'Today':
                filterDate = currentDate;
                break;
            case 'Tomorrow':
                filterDate = currentDate.setDate(currentDate.getDate() + 1);
                break;
            case 'NextWeek':
                filterDate = currentDate.setDate(currentDate.getDate() + (1 + 7 - currentDate.getDay()) % 7);
                let startDate = new Date(filterDate)
                lastDate = startDate.setDate(startDate.getDate() + 7);
                break;
            default:
                break;
        }

        filterDate = new Date(filterDate)
        if (lastDate) {
            lastDate = new Date(lastDate)
        }

        this.setState({
            filteredSessions: sessions.filter(s =>
                s.doctor.name.toLowerCase().includes(this.state.searchName.toLowerCase()) &&
                s.doctor.speciality.toLowerCase().includes(this.state.searchSpeciality.toLowerCase()) &&
                (date !== 'NextWeek' ? new Date(s.date).toLocaleDateString() === filterDate.toLocaleDateString() :
                    new Date(s.date) > filterDate && new Date(s.date) < lastDate)
            )
        });
    }

    filterSessions() {
        let sessions = this.state.sessions;

        let filterDate = new Date();
        let currentDate = new Date();
        let lastDate = null;

        switch (this.state.searchDate) {
            case 'Today':
                filterDate = currentDate;
                break;
            case 'Tomorrow':
                filterDate.setDate(currentDate.getDate() + 1)
                break;
            case 'Next Week':
                filterDate.setDate(currentDate.getDate() + (1 + 7 - currentDate.getDay()) % 7)
                let temp = new Date(filterDate);
                temp.setDate(temp.getDate() + 7);
                lastDate = new Date(temp);
                lastDate.setHours(0, 0, 0, 0)
                break;
            default:
                filterDate = null;
                break;
        }

        if (filterDate) {
            filterDate = new Date(filterDate);
            filterDate.setHours(0, 0, 0, 0)
        }

        this.setState({
            filteredSessions: sessions.filter(s => {
                let sessionDate = new Date(s.date);
                sessionDate.setHours(0, 0, 0, 0);

                return ((s.doctor.id === this.state.searchName || this.state.searchName === -1) &&
                    (this.state.searchSpeciality === -1 || s.doctor.speciality.includes(this.specialities[this.state.searchSpeciality])) &&
                    (filterDate === null || (this.state.searchDate !== 'Next Week' ? (new Date(s.date).toLocaleDateString() === new Date(filterDate).toLocaleDateString()) :
                        (sessionDate >= filterDate && sessionDate < lastDate)))
                )
            }
            )
        });
    }

    clearFilters() {
        this.setState({
            filteredSessions: this.state.sessions,
            searchName: -1,
            searchSpeciality: -1,
            searchDate: ''
        });
    }


    componentDidMount() {
        this._isMounted = true;
        if (this._isMounted) {
            getAllDoctors()
                .then((res) => {
                    if (res) {
                        this.setState({ doctors: res.data });
                    }
                })
            this.loadSessions();
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }


    handleClickOpen = (data, index) => {
        this.setState({
            open: true,
            openedSession: data
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
            openedSession: null
        });
    };

    saveChanges() {
        this.setState({
            open: false,
            openedSession: null
        }, this.loadSessions);
    };

    handleClickOpen2 = (data) => {
        this.setState({
            open2: true,
            openedSession: data
        });
    };

    handleRequestClose2 = () => {
        this.setState({
            open2: false,
            openedSession: null
        });
    };

    handleDateFilter(d) {
        let currentSearchDate = this.state.searchDate;
        this.setState({ searchDate: currentSearchDate !== d ? d : '' }, this.filterSessions)
    }

    appointmentOnSave = () => {
        console.log('appointmentOnSave')
        this.setState({
            open2: false,
            openedSession: null,
        });
        this.loadSessions();
    }

    render() {
        return (

            <div className="mx-5 mt-4 h-100">


                <div className="page-heading d-sm-flex justify-content-sm-end align-items-sm-end">
                    {/*<h4 className="title mb-3 mb-sm-0">Seach your Session</h4>*/}
                    <div className="row">
                        {/*<Input*/}
                        {/*    placeholder="Search"*/}
                        {/*    className="mr-5"*/}
                        {/*    inputProps={{'aria-label': 'Description',}}*/}
                        {/*    onChange={(event) => {*/}
                        {/*        this.setState({searchName: event.target.value})*/}
                        {/*    }}*/}
                        {/*/>*/}

                        <FormControl className="mr-5">
                            <span className="text-light-grey font-size-12">Doctor</span>
                            <Select
                                value={this.state.searchName}
                                onChange={(event) => this.setState({ searchName: event.target.value }, this.filterSessions)}
                                autoWidth
                            >
                                <MenuItem value={-1}>All</MenuItem>
                                {this.state.doctors.map((d, index) => {
                                    return (
                                        <MenuItem key={index} value={d.id}>{d.name}</MenuItem>
                                    )
                                })}
                            </Select>
                        </FormControl>

                        <FormControl className="mr-5">
                            <span className="text-light-grey font-size-12">Speciality</span>
                            <Select
                                value={this.state.searchSpeciality}
                                onChange={(event) => this.setState({ searchSpeciality: event.target.value }, this.filterSessions)}
                                // input={<Input id="age-simple" /> }
                                autoWidth
                            >
                                <MenuItem value={-1}>Any</MenuItem>
                                {this.specialities.map((s, index) => {
                                    return (
                                        <MenuItem key={index} value={s.value}>{s.displayName}</MenuItem>
                                    )
                                })}
                            </Select>
                        </FormControl>

                        <div className="my-auto">
                            {this.searchDates.map((s, i) => {
                                return (
                                    <Button key={i}
                                        className={`btn-lg border-primary ${this.state.searchDate === s.label ? 'bg-primary' : 'bg-white text-primary'}`}
                                        color={""} onClick={() => {
                                            this.handleDateFilter(s.label)
                                        }}>{s.label}</Button>
                                )
                            })}

                            <Button className="btn-lg" color={"secondary"} onClick={() => {
                                this.clearFilters()
                            }}>Clear Filters</Button>
                        </div>
                    </div>
                </div>


                <div className="row">
                    {this.state.filteredSessions.map((data, index) => (
                        <div key={index} className="col-sm-6 col-md-4 col-lg-3 col-12">

                            <div className="jr-card text-center">
                                <div className={`jr-card-header-color bg-gradient-primary`}>
                                    <div className="jr-card-header-top d-flex">
                                        <IconButton className="jr-fs-lg text-white" aria-label="Menu"
                                            onClick={() => this.handleClickOpen(data)}><i
                                                className="zmdi zmdi-menu" />
                                        </IconButton>

                                        {this.statusColors.map((s, i) => {
                                            return (
                                                data.status === s.status
                                                    ? <Badge key={i} className={`ml-auto bg-${s.color}`} color="success"
                                                        pill>{s.label}</Badge>
                                                    : null

                                            )
                                        })}
                                    </div>


                                    <div className="media user-profile">
                                        <Avatar
                                            alt="..."
                                            src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMxaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCMUM1NzBFREMxQzExRTg5NjRCOTQ2NDIzMUZCRTgyIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCMUM1NzBEREMxQzExRTg5NjRCOTQ2NDIzMUZCRTgyIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NUU4MzQ2NEVCRTJFMTFFODkzODRGNkFEOUQ5MUM4OTUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NUU4MzQ2NEZCRTJFMTFFODkzODRGNkFEOUQ5MUM4OTUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAAGBAQEBQQGBQUGCQYFBgkLCAYGCAsMCgoLCgoMEAwMDAwMDBAMDg8QDw4MExMUFBMTHBsbGxwfHx8fHx8fHx8fAQcHBw0MDRgQEBgaFREVGh8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx//wAARCACWAJYDAREAAhEBAxEB/8QAqwAAAQUBAQEAAAAAAAAAAAAAAwIEBQYHAQAIAQABBQEBAAAAAAAAAAAAAAACAAEDBAUGBxAAAgECBQIEBAQDBQYHAAAAAQIDEQQAIRIFBjFBUWETB3GBIjKRQlIUwSMVobFiMwjhclNjJBaCorJDc0QlEQACAQIEAwQJAwQBBQEAAAAAAQIRAyExEgRBUQVhcSIT8IGRobHB0TIG4VIU8UJyI4JiojNTJBX/2gAMAwEAAhEDEQA/AHlncRTQF0OqPNQRWgpjs5xaZxdq4pKqyEgOwC0prIFOmQw42LHAUKdKJpDZayKVoOw7DAEncMppC03pKtXr9WXQHE0VhUrzlV04g7Wsia5SFbVRgPqzUnIYKWGRHbdVVhWAQJSpY5geBPfA5hPCh0QhTGg+/rqJ6+dMKo2mlEAVfUkIGplVqnSBU98SZIhrV0G8txbI7ozekAhLOSNK5d2PT54fJVYCdW0kNUms54oTBPDPVVpGsqFmYdRme+DVxY0ab70RStSw1JpdzGaTWk13B6T6pgHL29Cso0tRgyH6qr3H8MPC/GSdHj7/AGDS284U1LBvPh2Y8nwYomCaD1IXEivqzHYjI1HY1wVu4pKqyIrlqVuVJLEHOFZiwrTLqanIYkiC5VY3kXIYIKLAMnl2w5ImAddX9uESpgnGeEGgeWnPphBA6HV54cM1W3jdYqMqZIAFjFEyFPnjGky9GLpw9WQkygKQGAcClFOdPjh6AuQx3a6KQLIkxUaqaQQSR3xNZhV0oVtzcoqphWkGhpVTRHoD0/MwArgUuAblhXhQj9ouVkhlFfr9Vm0dgp75eeJ7saNFWxPB88yRdWij1Vao61yoPicQp1ZPLBFW5Xvm37UiSz3kttP0Dwx+uQT/AMRGpk3liPcbuNqOLp6qhbXYzvTqlVd7XvM4n5xyS7uGSDcyiiumSFGj151V0Rs0bxAOOevdRut4S9PkdNZ6ZYisY+nzGA3fkFxDLHdXMl5ApofVY161+lxnTyOXjipPd3ZKjk2i3DaWousYpMZPb7Y0AnZAkZYK8qAKY3PTWFzT/wBJ7HFepYCf1PcbW59CWeR5PutpwxJJVaKNVa5jLriVXpp1TdSJ2YNUoqExxnmLbfdkXjvJbz/zZ2JqzBsvU827H4YvbDqErM6yxi8/qZ3Uulxv26R8M1l9O40lWjmhjuImEkEyhoZB0YHvjsoTUkmnVM4OcJQk4yVGnRg2U5jw6fPEgkwLLkRWvb5YRImAdeww5ImN5O5+f4YcliCIyz+OEGgdPqphwjTUuJA7IXU1yjKN0HeqjGQ4qhbU3WlRmyOb/wBVQfSIKE5CtMSp+GhA09dVkFkihuFRZIxKB3HgOuBTayClFSpVVBXIneN4o1CoFK0HUsf4DBwonVkd3U00sgW0IIYZI5KBkYO3zFcFdxdSKw6Jp8BN7fHVJr0xQxHXLLK9FFOmonIVwklGOpv5ATnKc9CTz737DEebcml3a9cPeRPBCzBBBEygk/4ySzj4mngMcpvdz5ks00uS+Z22x2vlRpRpvm/lwK/tlot36sinV6ADyRrXUV7kDrl5Yz3I0KEkY9wiiR0nS40jVBNEwMujzX/3AOh/MMBUegzhuJXlS7iSkzMYr6ACqOhpmU8DXMeOeEKgqZAIgik+nby6oAcyEahofHSajDoZjdpIxcVP2KXC5/lJJphxFp4hy8bTcQ2t+3/594w1Z/5Dk5OB+ns4+eNnpPUPKlok/wDW/c+fdzMPrXTPPhrgv9kf+5cu/l7DTWAOYIK9iDUEeIIx1xw6AOMyfHDkiAsCAT3HT44ckTGzr0yw5KmCIy8DhBoDp+qnywiSpoUNxA1NINdIqmdenemM6UWSQuReQFlHrjKihSXAahqemWCWRE0tQYTMsSxh9GrqadfKgwOnGpJroqVoJ9aURs8oLKv0qK0JoKnph9KrRAu46VY3tlnkuXqDEuhKk+AqM6164llRLmVoapS5CJbXb0ivLje/TFlbqJXaajIqE09T0z93TLLEN9wcVWlFzJ9rG5GTpWrwVONfefPHIb03u+XU0Q0QPIVtV0hKRdY/pHSqmpxxu5veZNy4cO47vbWfLgo8ePeXzjHs7y+9ginjs5LYyKAHYFCwbMVU+GMye8gu00o7ObLBB/pk5rOZmlmitpFIcaWqjg9xTo3wwH81cEH/AA+0i999uOQ8fStzA1xNqeBJ9BDMAAx108DkDh430+wUtu12kbtvtPzTdYZJI9tljjYeoHppWnmTTPBvdQXEj/iTfAgt79u+R7ax9W2bQO1DiWF+LI5beS4FZvknSQR3CsrLT7hnieNGQtNGk+2fIGu7BtnnYvPaK0kDsRnBVQEHf6ST8sdZ0bda4eW845d36HF9f2Wifmxylg/8ufrLmy42zATBSDDhxY2k6HL44ehNEAw8MOSIHQa/LxwguBcmvp1aIQsKH6aqCa9syeuKStrGoL3Ek1pYX1gPoDgjqQaVHmx8TgdIeumFRrLLNr1KwKmgHgScSKKIJTdcw8cklBmFOQBUVr5ZYFpBKTFW1wizlSQxKEtIetQ1fhhSjgDC4tXPDMzr3ou30WBWpDh9UmoMCBSkZAzA/NQ5VzGMLrNVGK4Y/wBDpOgNSlN8cP6jT2L41Dv3M3vruMSRbTD+4jVhqBuGYLDUHI6fu+WOS30mo0XE7LYwTlV8D632+BYURa1oKlvE9STjJSoajxJqKKP0wTLpp8P4YtwiqZlaTdchO429lcWxglCTIy9GpqFDXKvngrlKUGt1rUhZWVENR9ooAoyoPAYp1LqVSs79HazxsJI1fV0ZhX+3tgovEdxwMK9weLWMqSyvGsYQM0TAZ/CoyyxqWblUZO5t0M89vLh7Tl9pGELC5127U/S4qG/8JFTjouj3NN+K/dVHMddtatrJ/to/Ya+woOuOxRwiAOMq4ckQBhngiRDdx44RKgR+4YcMtSmOMmhIAJ6A5fAYqurKyaTOMbYtWh0qNRAORPn/AHYSqM3Bs7HEjAMNRIOZAzPkMJsZRTxCJXouTE6UXVlTv1/vwzCXYDaZTKqKF0hWBYCgpkTSvfBKOFSKUlkZv7q2807i7QiSztl0SgR0Mcr/AGB5PzEjtXLGD1uDaT4LP+p0/wCOXIqsf7pYrHNLsLB/puuZIt0uYzq9AoDIAKirGgJPYAY43e5HdbDNn1BZReqmrqOg+GMuMamjKVCXh2y3MYZ2KA5jwGLcLEaYsrSvyrghEu2mo0yKyCp1HI07YUrHJijf5oY3lrAiAFgzscqeOIZW0ia3cbK5uu3SzR6I1KsCKVHSmI0qE+tGNe4snqI9isemUhiKdDTuMX9quJS3WKoYft16du36zvidJt7gNIR1I1BXr8VONvZ3vLuxlyaOe3tjzbM4c4s2xwK0U1Xs3anamO+PNEN5O+HJEBbBEiG70rhyVAvz4QfAssxpMVK6dJGfU5d/OuK0cipPB0AyymtD9xNSCKGuDSAbbCC7fJs9VTQ16ZUyAw2gd3Ge9SSUBdQCDIKTSvfPCokC5Nqh4xnUtCPoB1VIp8sOmBQq/ucrNwydtQCpNEaUOZLUy/HGb1lf/M+9Gv8Ajzpu1/iyV/062dvDsm67vM4UCT02b9EUSiR+vSuPPt1jJI9S2WEWy1HmvvZyyY/9i7X/AE2wlqI7m59MemgyBLPqz7/bipGME/E8ewnnKTWCVO0hrXYPfrj29Gfe9w/qcE51XaW956rKK1LKgppNfKmFcu2aUSow7Fu63Wqa9nyPoDjs13dbIkvqNI/pDWzijFgM6jpXA26uLoNeopKqMS92eecvk35eOcUaRb6KMpcOjaFUv3LHoQBg7SjTVPIa5qyhmQFnwH3oubKF7zmVqkijVHaNeNJPTrRgBXP54J37Sx0uhGrFzLViVbeZ+W7bfCy3x0uEDMILiIGiyfmDVGoasXdtpllxK9/XD7jOuQjTuk4GQYB6dqsueLNKYFKWZsm0XjXWzWFxSjS28RIrUVCAfwx6Dt56rcZc4r4Hme6taL048pP4h5DQUOY8cTkUUCJGQ7nBBoBJStBhIliBoNQ8cOGTryMzkrUKaUBxEkUZME5atTmT3w6EhSOoNK/HthNDNB4/Q+osKt+UDpgXUbhicPYk5HoMEARvLNk3DfOMX9jt9rLd3Kx/uBFCjSPSEhjkvbGd1aUFtp62or58jW6Dbm93HQm8/ZzHX+l6wN9te921wmu0/cxjQ4qhdo21Ag5dFGPPN26YnqWzTozR+W7dza7nfadlvRx7Y1gc2t9AFE8t3T6dQNAkSt1I+qnTGfYlBYyxNG7alKPgaT9MOwqnC+EbzFuszch3c3+8PPDJGti80lpDDEtJ/VDKpJnHcEaT9XXLBX7tueEULbbe7bWqcsDatkjMdnLHAWWAE+kHNW0jpU4CzF0YrtKquZi97sO3P7h7vFdRPcG4niuQoJHqxJnLb5MuUhoCa101A64JtpLjQNwTb4VITkPtdxa43ifc459zis4/Waw2QxmttLMS2mOVauY0c1UUr0FfGzHfcKIqf/lpOrl7Be0cN39eMy2u8lp3erRTzH+aFUErUnvTAfyIq4nHIl/jtwo8zIN74vul7ylLC2TVJPGHZ2+lI0TJ3c9lWlTjVuTSxMWNpuWk0yyhitdutoYHaW3hjWGG4dPT9URjT6iqSTpJGVcdj0ffefZxVHDwv2HEde6d/Hv1T1RueJcOOP6CySWONdGKDYnxwQSQBsOSIFX6sOHQnNQzqPmcRmfQS7J0rn5YdCSZyMkt0+eHY7HKtGpzXWaZUNM/HAOoC7TgoBUnUa5/E9BhwWaF7TbZaXtzePKshntmt5IdDlCGq9D9JFaU6HLHIflqbVpcKy9tEdz+FT0+d2qK9WPzoTnGLbZIuV8nXaRFpl3FWuhDpCiYWya2+mgBJqW88ce6StqmVTuknFuqo6IuvoQSKFMayVyoRX+zEehchamuIG6tI7e1KJGkAfqEAA+LU64eUaKmQcJanXMHbAIjBFIULpXw6d8Pkh26vEyDkjNZ8/jmZdLRyRlh2ZX+mh+OH01tk9fEalBFCYI3UmhWoB6jxy8sVEqoZ1qRPIzbCzeIhVJFFyoanw+OJIxxGyVTAdvnE3uP+1/atdWqRk7gsZXUsQlGhmB+5PUpqXwxq7h6bSZmWVqvNFs5/aW9ithbKAs8Rn1BRSsUjLJGf/Owxv8A4ncbV3l4X66NHOfmFuP+p8fEvVh9SpCXPzx2KZxOk40gpghKIBpKnBIkUQeo1B74cOhNlvAADxwxn0OlqDIfh1whqCUc6qmp8sPQdocqQOgoPA1r8cCRMXUEeVcsIE0L2jvoobnc7ZWC3U0aNCWNBRdSnP4uMch+Wwl5duSyTa9by+B3H4VOGq5F5vS/Uq1+KJbd57bYfcbbo40MEG97Q5mRgAGubOYVk1dC3pzmvwGOQjBK3RcPmdu5t3G261+RdoLpRppSpHY9/niHXRkrjVAd0vUFo11KpFlCS0zAFiFTMtpGZA8sOpan2B24UdOJWeNe6/CN2lu4bKaV44xqFxNFJDDLTMmJ5AurTiemlYojcXJ4Mybn3utxubmN3Z2kEsqJGbdrhE1iR+oUEdKHEsLGFWJ7lRlpNX4lu13cccs5LpSLpYk9Ud+lPq8/HGZLBtFyUVmQfM939OD63qKk18KGlMT2FVkN50Rjnt7eyjn/ACC6WQjTEqAqutjUklF7eeNDdRrFIztpOk5MmOf7s13v7ayBJDGiMgbVooPpRiMtemhemVcsdL+M2NFhy/e8O5fqcz+U3lO9GH7Fj6yurP4nHTxkcs4Cmkqta/jiRMZREF8EmPQ9r88EPQnB1qfww5nMVQ55j5YQJxMm8+2HHY6UZCpzPc4AhZ1RTUSep/hhxmx7tO5Xe17hBf2jaZ4WqAc1YEUZWHdWGRxX3W2hftu3P7ZelfUWdnvJ7e6rkM4+lDnut7kQ3W6cOv4rWS2uLWe5guTUMpS4VB6aV7MVzJAxwm96LPaptyUoyy54c0ei9P67b3b8KcZxzXfyf6Fvi91rKx4rue9XEqyDb9SxaauHnA+lSfp1guaeGOddqTmkuJ0quR0t8EYRe+6Putyia8tbSedi8LCS3goKQk6mYhaKPCvyxeW3twpUprc3bmEUP+CcP9y91jNhI8S7U2k3ZvrlIwFUVCxNXVqp109MPOcOGZJb2u4pinQrPIfbzk2yyy3Ty28uiUxVsrpJWJILBlzVmXz8euJYXoywIbuxvQ8TQ/4t7qct2aUCdmujCpSeCSoLxlaAkDqQPLEdzbRkhre8nB44lv3/ANwYbzbS8L60uY9VGYMUIUUFR4YjtWNLJb+5UlgUvh2/7jZ7jutzZsEMwjUy0qQy6hUV/wAJxvbLYwv114qJg7zqFzb/AGfdL0wJL1WZizMSxJLMc6k9zjqIUSosjmJtybbxbFq/nliZMjaCerRfEE54kTA0nvVrg0xaTuvLB1FQsVT2/HEplHhWv8cITFKADmc8IFh0avTP44YjaCIQdXkcMC0EBPzOECU73VtjJx+C4DUFrcA6SOusaevamMXrkK2E+UvidB+NXKbhx/dH4FWHIri949d2ORilaKR0J6PEaKxr11dMscQ7aUqnf+a3Fo3H2a9mOFb17fWe6bva/vNzvmeWWT1HVUAchYnVGWooOh74o3bsvMdHgjR2+mEVVJ1z9Owvn/a3tnsFrJZ3MNpavJREGplFOgXSK164dXE88zRW7eGnwx5YFc5CntxcznabOGzWX7Fs0hV5CcvzEVwnclmsgv5MaUfikVD3H9vOE7fwS5mtrdLfc4yrWlxFk7yaqyRse9V6A4azdm7meBnbqEXBuiTMJe8dVCxHSmYUVqufUgdvhjUoY1SW4oCLK4kbIyTdf90f7cdD0qNLbfaYPVHW4l2E4GxrIzWgoOJIkbFVr8j0xIhjtcGhqCtRp54MVCy5/LFkyBSUBwgWEIqRXp3OGBCoBWvbDAMOpJFK5A5YYjYsDzwgRvuWz228WUu2zj6LkBA36X/K3yOK+6tRuW5RllQtbG/K1ehKOdfjmYfeWN5s99dbbeRGK6tZXinjIoRIh00IOenvjz1YpPmeoSWltcjRvaL3V33ju72e3Gcvs9zcJDPa1pQUpkx6DP54p7jbp+JZlrb38dLyPoXmPCeB8+a1uRfNb3NqwIKOqFjpKoHU5ihzy64qQdMY4F5p0pJVK7Yez3H+L7xbbjc7nLMizNKkzMqhSkOkK5BqOhofxw9yUmqBW9OazMo91fcO33jc7jb7GVJ7Bhqgl/Jq6MVK50JGYPxGJ9tYaxeZU3W4/tWRkEkkhkJYUft5knw88aKRnFz2qwFlt0UJqXYeo583zpjpemx/0rvZgdQr5vqQ7FaeWL6KLCKcGgWEU/2YkQLOM+eWDQkjurLzwYqFp/ji0YrFjI5ipwhhQDZf3eGECFTCAYaMrn5YFkbDLXAsBls9vOMR77vbG5BO3bdEbq8ANNQX7I69tTdfIHGX1XeeTadPulgvqa3Q9j/Ivqv2xxf09ZXvfP2km3kS8s2VS+6emp3KxAP88RrT1IqD/MCfcPzAeOPO9rf0+F5HqW6savEsz57tIvUCQagkhcFHrSmkVYVyoB+ONBozk8S4cf8AcHcrOK6tymd1LGZc9JaKEUSJf0orUYkGrYqzsF23uWhxyL3H33c1ljac67lpXmjQ6Fq6BFCZ5U0Ajzw0LCQp7lsz5vUjzLUVs9ZHc9PxxbSKTY72iyeaQ3Uv2itGPiT1H9uCQjSLrjV9bcZsd+++yuZ5bOQnNo54gGAc/wDMQ1HzGOh6VeUoaOKxMXqVlqergyH7406GaKA88Ghjtew/DBoY8TngkIVXLpgxqFq1Dti4Y1DymhJrnhCaFhq9c8IBoNGR264YCQdHAB7CvU4ZojaNI4D7U7ju8sd9vUMlntNA0cROie4J+0AdUTuWOZ7eOMLqHV421ptvVP3L6s3emdDldaldwh736f0L7xM7dNsO/wB3s9tDZ7Q0z222rCorLDbMYmuHkqWf1ZRJpr0UDxxyu+vzmvG25UOu6bZtQm1BKKqvSvE5cWzvbNGn0hhSufw7YwKHRNnzz7s+z901wd542qwmZybm2WianNfriAFASag4vbfcYUkUr+2rjExy82rd7Msl1bPHISaMPtYoK0r8Di4mnkUnFrMbzx3sq0W2LMykNIM6dCp+VMFQYcWezvcMovZKIT9EYqxaoyoR4nthnKgUYFlksFtJLSN9AR1BUNSilKrWvYLWuI4yrUkmtNDbOGccbfvZTkdhbzxyltyT+m3Mh0RGeBYgfqPQV+muL+wvaLqkylvIKUGq1yMc3Lb9w2zcLjbtwha2vrVzHcQPTUrD+8HqCOuOshJSSaxTOdlBxdGN1Y4NANCg2DQ1D1RXDoVBWrBjULRU5jF0xqHQRXCEK1ACpIAHfCoNQuHFvbTl3IYlube3FnYOaLd3ZMasPFEprf5CmM3d9Vs2XRvVLkvSiL+26TevYpUjzZs3DvarjnHgtxKv9T3LL/qbhVKIw/4MeYX4mpxy+96vdv4Lww5L5s6XZdHtWcX4pc38kXhBV8yanKo655YyzWKF7KehN7abXYnpFbC2lHf6WeNvnqVsPdVZSXpkVNnPGS4pr4D+S3kgieJ8p4SY3Y9KD83wK54xXHTg+B0SlqxXEhtx2tLpNVaaGDrpGRKjJjTvq6Yao5n3JuNwsrtcwB0iD1kCUp6gzIp93WuZwcZjOFTNpeBMtyJY20RxkF0IJJ1ZjLwyrniyr2BXdjEFDwt0vogBWKvqxlPpJB6gE9M8hh3ewErGIfcuK3t5ullZRwl7i+kjh22BUH1EsRJCsgow7O1ctNT2wrN3lmBuLWPiy9MDX/cXZ4eA+wkux2E1ZrNYGluRkZLg3K3Er1y+90oPLGlbzqZV7BJc2WHmXt9xnldhc3d7ZLFul1DH6O4otLiEqoKjsG09CrYt2N3O1JJPwrgR3dvGaq82YnyT2N5jtKLLtunfISaOlspSdR2JjY/UD4qcsbNnqVuWfh7zMubKUcsSibht+47bdmz3G1lsrtczBOjRtTyB6j4Y0oTUlVOqKkoNZjbVXBjUF9sGCWrF4xWS3G+Kcj5Jcels9i9wisFluD9EEde8kp+kfAZ4rbneWrCrN07OL9Ra2+zuXXSKN14R7P7Bx/Rd7lp3bdxmJZF/kRH/AJUTdSP1tn8Mclvus3L3hj4Ie997+SOn2fSbdrGXil7jQVWp1Nm3SvgPAYxjXCAADCEeXKQHwNcIRnftVKLHc+R7AR6f7Dd9xhjVv0PML+3A+MN6fwwrj8a7V8Chb8F+nCcffF/Rl33iyMifu4l1SRr/ADUAqXQZ5f4l6j8MVdzZ1KqzRsbe7R0eTIBDDp9SIqVkGoN1rXvjNUlwNFp8SOvFhOqqhhmDUVoDgXINRK9vMG2FTWFS5BFFAGRHlhtZIoERY8fhes0sOouQkaAEu1cgAO5OGlcbwQWhI0jjXErbZE/qV6iNuZQrEKAi2QihVD+thk7fIZY1NpttGL+5+4x93uVPBfaig+4rjlfM+O8JQa4ZrxLzdwK6VtrQLdSo1PBAg/3pAMX4yx/x+Jjyeu5T9vxf0WPrRq00Ky+pqFC5LNTpnnhi1Qj5IXidFP2k01eAwdQKDPk3FNo5HsNxtu62yXEbRuLaVgDJDIVOh4n+5SGzy64ksX5W5KUXQG7aU1RnyLyLiPJOMzJHvNm8McpKwXYFYJSOulxlq8VOeOvs7iFz7WYVy048CM1/TiwQUPo7hnsNOzwX/KpVVAVf+kQHUW76Z5egHiqfjjI3vX1Rxsr/AJP5L6lnZ9Fylc9hs1tY21rbpb2sKW9tEKJBEoSNR/hVaDHMym5OrdWdBCCiqJUQZABmBgQ6HnH1Dzwwgq9MIRwfdhCMjv7w8f8AfLefUBEW8bZY77A9fpL7aWsrxQP1G1kLfBRgb32J/t9H7mZ++8C8z/1yUv8AjlL3VNiBHUGqnMEeGHNApXJNquttvv3lqC213TfzkH/15m/P/wDHIev6W8jjJ3u30vXHJ5/U1tnuFJaJfcsu3sGUsU6CpQnx8cU8UW1RkeNruLicLHCzsxooA7nAJSbokSOcUqtlx2LjcG1KLu6IkvQKJ+mIEZhfFvFvwxsbTZ6MXjL4GPut55mCwj8RtyHc5oLKa9WL1zGVisrQ5fuLqVgkEXwaQjV5Vxcu3NEarPh3mPub/lwcvYubeSM89lLSPcuScr5SZf3cMEq7Dt24EUNwYWNxuNypBIK3F04YU/KFHbBxhpil6elQrFrRGjxfHtk8/Tka1qRTnXPvhycRJArCooQeqnocIVDvpADSMh+IwhEFvOyWW4ymz3C1jutuuFK3NtMuqNmA+k08fMYmt3XHFOjIZ203jkZlP/pz4wOQ293Bcy/0QuzXezyM1dOklVinB16demob8tc8aq6xPQ014uf6FN7Faq8PebqtKCnSmMM1BX5R3wwhEf2n45YQhT9BhCCL9uEI4MIRk3viII974VfbaUn5hZXk8u37UfpN7tpipucJkakUYEP1apXVetM8FhR1yIb+jS9X20de40ripuf6BaJchv5S+nBK9NUsC/5EhHUM0enUGoa1xBarpRB07V5EVLhhXmuD9a54kjM6qja4y8dD6ldITTTPVqIywbLqIN49qaNWhldFP2rQsAvlrAy+eKEo2Xk6F+MrqzVSSsf2YGm2AZ6fcaaiMWrKh/aVr2v+47MFLfzmoPniwiBmde8d1ymPZXHHLNp7z0ZINmKMq/8AWTxP61zmwOq2tUf0lpqZ2+lTTED/APIq+rv/AERnXsdxDXhBfb/1XH8NKrStKt4Er7QW3FLb2z2G34vdpf7PDAUS9RHjE04c/uZCkipIpabVkwriwy+i1S07YQ51CadMu+EIU3nhCEyelQa88IQ2ZYPXUhvGq/Lxw42B/9k="
                                            className="user-avatar size-50"
                                        />
                                        <div className="media-body align-self-center text-white">
                                            <h4 className="mb-0">
                                                {data.doctor.name}
                                            </h4>
                                            <span
                                                className="meta-date-light jr-fs-sm text-white">
                                                    {this.specialities.map((s, i) => {
                                                        return (
                                                            data.doctor.speciality === s.value
                                                                ? s.displayName : ""
                                                        )
                                                    })}
                                                {/*({data.doctor.speciality})*/}
                                            </span><br></br>
                                            <span
                                                className="meta-date-light jr-fs-sm text-white">{data.doctor.telephone}</span>
                                        </div>
                                    </div>

                                    <Fab className="jr-badge-up bg-success" disabled={data.status === 'CANCELLED'}
                                        onClick={() => this.handleClickOpen2(data)}>
                                        <i className="zmdi zmdi-account-add" /></Fab>
                                </div>
                                <div className="jr-card-body pt-2">

                                    <div className="media">
                                        <div className="media-body">
                                            <p className="mb-0">{data.date}</p><span
                                                className="meta-date meta-date-light">{data.start.substring(0, data.start.length - 3)} - {data.end.substring(0, data.end.length - 3)}</span></div>
                                    </div>
                                    <br></br>
                                    <div className="media">
                                        <div className="media-body">
                                            <p className="mb-0">{data.room.name}</p>
                                        </div>
                                    </div>
                                    <div className="media">
                                        <div className="media-body">
                                            <p className="mb-0">
                                                {data.appointmentCount} Patients
                                            </p>
                                        </div>
                                    </div>
                                    <div className="media">
                                        <div className="media-body">
                                            <p className="mb-0">
                                                {data.confirmedAppointmentCount} Confirmed Patients
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    ))}
                    {this.state.openedSession ?
                        <div>
                            <SessionInfo open={this.state.open} onClose={this.handleRequestClose}
                                onUpdate={() => this.saveChanges()}
                                data={this.state.openedSession}
                                title={"Session Details"} />
                            <MakeAppointment open={this.state.open2} data={this.state.openedSession}
                                onClose={this.handleRequestClose2} onSave={this.appointmentOnSave} />

                        </div>
                        : null}

                </div>


                <NotificationContainer />

            </div>
        );
    }
}

const mapStateToProps = null;

function mapDispatchToProps(dispatch) {
    return {
        dispatchSessionData: () => dispatch(getSessions())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
