import React from "react";

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from "@material-ui/core/CircularProgress";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ComponentToPrint from "./ComponentToPrint";
import ReactToPrint from "react-to-print";
import TextField from "@material-ui/core/TextField";
import DialogTitle from "@material-ui/core/DialogTitle";


class ConfirmationPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            confirmText: ''
        };
    }

    isConfirmButtonEnabled() {
        return this.state.confirmText === this.props.confirmationText;
    }

    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"sm"} onEntered={this.onDialogOpen} open={this.props.open}
                    onClose={this.props.onClose}>
                {this.state.loading ?
                    <div className="absolute-fit d-flex align-items-center justify-content-center bg-transparent-white">
                        <CircularProgress/>
                    </div>
                    : null}
                    <DialogTitle>
                        Confirmation Needed
                    </DialogTitle>

                <Divider className="" light/>
                <DialogContent>
                    <p>
                        This action will send sms alerts for all the patients in the session.
                        Please type '{this.props.confirmationText}' and click confirm to proceed.
                    </p>
                    <TextField
                        id="confirmText"
                        label={"Confirmation Text"}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={this.state.confirmText}
                        fullWidth
                        onChange={(event) => this.setState({confirmText: event.target.value.toUpperCase()})}
                        margin="normal"
                        type="text"
                    />
                </DialogContent>
                <DialogActions className="mx-3 mb-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                    <Button disabled={!this.isConfirmButtonEnabled()} onClick={this.props.onConfirm} className={`px-3 ${this.isConfirmButtonEnabled()?'bg-primary':'bg-light'}`}>
                        Confirm
                    </Button>

                </DialogActions>
            </Dialog>
        );
    }


};

export default ConfirmationPopup;
