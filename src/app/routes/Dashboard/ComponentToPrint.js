import React from "react";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Select from "@material-ui/core/Select/Select";
import MenuItem from "@material-ui/core/MenuItem";

class ComponentToPrint extends React.Component {

    session;
    patient;

    actions = [
        {value: 0, label: 'Confirm', disabled: false},
        {value: 1, label: 'Refund', disabled: false},
        {value: 2, label: 'Cancel', disabled: false}
    ]

    constructor(props) {
        super(props);
        this.state = {
            actions: this.actions,
            action: -1
        }
        this.session = props.data.session;
        this.patient = props.data.patient;
    }

    componentDidMount() {
        if (this.props.data) {
            this.actions[0].disabled = this.props.data.confirmed;
            this.setState({actions: this.actions})
        }
    }

    render() {
        return (
            <DialogContent style={{width: '50vw'}}>

                <div className="row" id="test">
                    <div className="col-md-12">
                        <div className="page-heading py-3 mt-1">
                            <div className="row">
                                <div className="col-12 col-lg-3">
                                    <label className="text-light-grey font-size-12 mt-3">Appointment No :</label>
                                    <h4>{this.props.data.number}</h4>
                                </div>
                                <div className="col-12 col-lg-3">
                                    <label className="text-light-grey font-size-12 mt-3">Date :</label>
                                    <h4>{this.session.date}</h4>
                                </div>
                                <div className="col-12 col-lg-3">
                                    <label className="text-light-grey font-size-12 mt-3">Estimated Time :</label>
                                    <h4>{this.props.data.estimatedTime}</h4>
                                </div>
                                <div className="col-12 col-lg-3">
                                    <label className="text-light-grey font-size-12 mt-3">Room # :</label>
                                    <h4>{this.session.room.name}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-n3">
                    <div className="col-md-12">
                        <div className="page-heading py-3 mt-1">
                            <div className="row">
                                {/*<div className="col-12 col-lg-4">*/}
                                {/*    <label className="text-light-grey font-size-12 mt-3">Title :</label>*/}
                                {/*    <h4>Mr</h4>*/}
                                {/*</div>*/}
                                <div className="col-12 col-lg-4">
                                    <label className="text-light-grey font-size-12 mt-3">Patient Name :</label>
                                    <h4>{this.patient.name}</h4>
                                </div>
                                <div className="col-12 col-lg-4">
                                    <label className="text-light-grey font-size-12 mt-3">DOB :</label>
                                    <h4>{this.patient.dob}</h4>
                                </div>
                                <div className="col-12 col-lg-4">
                                    <label className="text-light-grey font-size-12 mt-3">Age :</label>
                                    <h4>{this.patient.age}</h4>
                                </div>
                            </div>
                            <div className="row">
                                {/*<div className="col-12 col-lg-4">*/}
                                {/*    <label className="text-light-grey font-size-12 mt-3">Age :</label>*/}
                                {/*    <h4>25 Years</h4>*/}
                                {/*</div>*/}
                                <div className="col-12 col-lg-6">
                                    <label className="text-light-grey font-size-12 mt-3">NIC :</label>
                                    <h4>{this.patient.nic}</h4>
                                </div>
                                <div className="col-12 col-lg-6">
                                    <label className="text-light-grey font-size-12 mt-3">Address :</label>
                                    <h4>{this.patient.address}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {!this.props.hideActions?
                    <div className="row mt-n3">
                        <div className="col-md-12">
                            <div className="page-heading py-3 mt-1">
                                <div className="row">
                                    <div className="col-12 col-lg-3">
                                        <label className="text-light-grey font-size-12 mt-3">Appointment No :</label>
                                        <h4>{this.props.data.number}</h4>
                                    </div>
                                    <div className="col-12 col-lg-3">
                                        <label className="text-light-grey font-size-12 mt-3">Date :</label>
                                        <h4>{this.session.date}</h4>
                                    </div>
                                    <div className="col-12 col-lg-3">
                                        <label className="text-light-grey font-size-12 mt-3">Estimated Time :</label>
                                        <h4>{this.props.data.estimatedTime}</h4>
                                    </div>
                                    <div className="col-12 col-lg-3">
                                        <label className="text-light-grey font-size-12 mt-3">Room # :</label>
                                        <h4>{this.session.room.name}</h4>
                                    </div>
                                </div>


                                <div className="row">
                                    <div className="col-12 col-lg-6">

                                    </div>
                                    <div className="col-12 col-lg-6">
                                        <label className="text-light-grey font-size-12 mt-3">Select Action</label>
                                        <Select
                                            value={this.state.action}
                                            onChange={(event) => this.setState({action: event.target.value}, this.props.onActionChange(event.target.value))}
                                            // input={<Input id="age-simple" /> }
                                            fullWidth
                                            label={"Select Action"}
                                        >
                                            {this.state.actions.map((a, i) => {
                                                return (
                                                    <MenuItem key={i} disabled={a.disabled} value={a.value}>{a.label}</MenuItem>
                                                )
                                            })}
                                        </Select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                :null
                }

            </DialogContent>

        );
    }
}

export default ComponentToPrint;
