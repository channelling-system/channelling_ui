import React from 'react';
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import TextField from "@material-ui/core/TextField/TextField";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Divider from "@material-ui/core/Divider";
import Autocomplete from '@material-ui/lab/Autocomplete';
import axios from 'util/Api'
import {Button as Button2} from "reactstrap";
import TableContainer from "@material-ui/core/TableContainer";


class SearchCustomer extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            patients: [],
            patientNames: [],
            filteredPatients: [],
            searchedName: '',
            searchedNic: '',
            searchedTelephone: '',
        }
    }


    openDialog = () => {
        this.getAllPatients()
    }

    getAllPatients() {
        axios.get('patients',
        ).then(({data}) => {
            if (data) {
                this.setState({
                    patients: data,
                    filteredPatients: data,
                })
            } else {
                // dispatch({type: FETCH_ERROR, payload: data.error});
            }
        }).catch(function (error) {
            // dispatch({type: FETCH_ERROR, payload: error.message});
            console.log("Error****:", error.message);
        });
    }

    filterPatients() {

        this.setState({
            filteredPatients: this.state.patients.filter(p =>
                p.name.toLowerCase().includes(this.state.searchedName.toLowerCase()) &&
                (this.state.searchedNic ? p.nic !== null && p.nic.toLowerCase().includes(this.state.searchedNic.toString().toLowerCase()) : true) &&
                (this.state.searchedTelephone ? p.telephone !== null && p.telephone.includes(this.state.searchedTelephone.toString()) : true)
            )
        })
    }

    selectPatient(data) {
        this.props.onPatientSelect(JSON.stringify(data))
    }

    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"xl"} onEntered={this.openDialog} open={this.props.open}
                    onClose={this.props.onClose}>
                <DialogTitle>
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">Patient Search</h2>
                        <div className="ml-auto jr-btn-group">

                        </div>

                    </div>
                </DialogTitle>
                <Divider className="" light/>
                <DialogContent>

                    <div className="row">
                        <div className="col-12 col-lg-4 col-md-4">
                            {/*<label className="text-light-grey font-size-12 mt-1">Name</label>*/}
                            <Autocomplete
                                id="combo-box-demo"
                                options={this.state.patients}
                                getOptionLabel={(option) => option.name}
                                onSelect={(event) => this.setState({searchedName: event.target.value}, this.filterPatients)}
                                renderInput={(params) =>
                                    <TextField {...params} label="Name" InputLabelProps={{shrink: true}} margin="normal"
                                    />}
                            />
                        </div>
                        <div className="col-12 col-lg-4 col-md-4">
                            <TextField
                                id="nic"
                                label={"NIC"}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={this.state.searchedNic}
                                fullWidth
                                onChange={(event) => this.setState({searchedNic: event.target.value}, this.filterPatients)}
                                margin="normal"
                                type="text"
                            />
                        </div>
                        <div className="col-12 col-lg-4 col-md-4">
                            <TextField
                                id="telephone"
                                label={"Telephone"}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={this.state.searchedTelephone}
                                fullWidth
                                onChange={(event) => this.setState({searchedTelephone: event.target.value}, this.filterPatients)}
                                margin="normal"
                                type="number"
                            />
                        </div>

                    </div>
                    <Divider className="mt-3" light/>
                    <div className="row">
                        <div className="col-12 col-lg-12"><br></br>

                            <TableContainer style={{maxHeight: 'calc(100vh - 300px)'}}
                                            className="table-container mt-n4">
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell className="table-header-cell-sticky">Patient Name</TableCell>
                                            <TableCell className="table-header-cell-sticky" align="right">Mobile
                                                No</TableCell>
                                            <TableCell className="table-header-cell-sticky"
                                                       align="right">NIC</TableCell>
                                            <TableCell className="table-header-cell-sticky"
                                                       align="right">DOB</TableCell>
                                            <TableCell className="table-header-cell-sticky"
                                                       align="right">Age</TableCell>
                                            <TableCell className="table-header-cell-sticky"
                                                       align="right">Address</TableCell>
                                            <TableCell className="table-header-cell-sticky"
                                                       align="right">Gender</TableCell>
                                            <TableCell className="table-header-cell-sticky"
                                                       align="center">Select</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.state.filteredPatients.map((p, index) => {
                                            return (
                                                <TableRow key={index}>
                                                    <TableCell align="right">{p.name}</TableCell>
                                                    <TableCell align="right">{p.telephone}</TableCell>
                                                    <TableCell align="right">{p.nic}</TableCell>
                                                    <TableCell align="right">{p.dob}</TableCell>
                                                    <TableCell align="right">{p.age}</TableCell>
                                                    <TableCell align="right">{p.address}</TableCell>
                                                    <TableCell align="right">{p.gender}</TableCell>
                                                    <TableCell align="right">
                                                        <Button2 color={"primary"}
                                                                 onClick={() => this.selectPatient(p)}>Select</Button2>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                        })}

                                    </TableBody>
                                </Table>
                            </TableContainer>

                        </div>

                    </div>


                </DialogContent>
                <Divider className="" light/>
                <DialogActions className="mx-3 mb-2 mt-2">
                    <Button onClick={this.props.onClose} className="bg-danger px-3 text-white">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default SearchCustomer;
