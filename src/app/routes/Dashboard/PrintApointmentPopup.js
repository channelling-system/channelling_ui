import React from "react";

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import CircularProgress from "@material-ui/core/CircularProgress";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import ComponentToPrint from "./ComponentToPrint";
import ReactToPrint from "react-to-print";
import PrintView from "./PrintView";


class PrintApointmentPopup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showInfo:false
        };
    }


    render() {
        return (
            <Dialog disableBackdropClick maxWidth={"xl"} onEntered={this.onDialogOpen} open={this.props.open}
                    onClose={this.props.onClose}>
                {this.state.loading ?
                    <div className="absolute-fit d-flex align-items-center justify-content-center bg-transparent-white">
                        <CircularProgress/>
                    </div>
                    : null}

                <Divider className="" light/>
                <DialogContent>
                    <ComponentToPrint hideActions={true} data={this.props.data} ref={el => (this.componentRef = el)}/>

                    <div style={{ display: this.state.showInfo ? "block" : "none" }}>
                        <PrintView data={this.props.data} ref={el => (this.componentRef = el)} />
                    </div>

                </DialogContent>
                <DialogActions className="mx-3 mb-2">
                    <Button onClick={this.props.onClose} className="bg-secondary px-3">
                        Cancel
                    </Button>
                    {this.props.data.confirmed?
                        <ReactToPrint
                            trigger={() => <Button href="#" className="bg-primary px-3">
                                Print
                            </Button>}
                            content={() => this.componentRef}
                        />
                    :null
                    }

                </DialogActions>
            </Dialog>
        );
    }


};

export default PrintApointmentPopup;
