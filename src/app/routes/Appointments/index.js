import React from 'react';
import AppointmentTable from "./AppointmentTable";
import {NotificationContainer} from "react-notifications";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import {getAllDoctors} from "../../../actions/Doctor";
import {getAppointments} from "../../../actions/Appointment";
import DatePickers from "../DoctorSessions/date";
import {Button} from "reactstrap";
import TextField from "@material-ui/core/TextField/TextField";


class Appointments extends React.Component {


    statuses = [
        {label: 'Not Confirmed', value: 0},
        {label: 'Confirmed', value: 1}
    ];

    constructor(props) {
        super(props);
        this.state = {
            appointments: [],
            filteredAppointments: [],
            date: new Date(),
            lotInfoOpen: false,
            doctors: [],
            searchDoctor: -1,
            searchStatus: -1,
            searchPatient: '',
            searchDate: null
        }
    }

    componentDidMount() {
        getAllDoctors().then(({data}) => this.setState({doctors: data}))
        this.getAllAppointments();
    }

    getAllAppointments() {
        getAppointments().then(({data}) => {
            this.setState({appointments: data, filteredAppointments: data})
        });
    }

    handleDateChange(event) {
        this.setState({date: event.target.value})
    }


    openLotInfoPopup = () => {
        this.setState({open: false, lotInfoOpen: true});
    };
    closeLotInfoPopup = () => {
        this.setState({lotInfoOpen: false});
    };


    saveChanges(data) {
        this.saveRoom(data);
    }

    filterAppointments() {

        this.setState({
            filteredAppointments:
                this.state.appointments.filter(a => {
                    return (
                        (this.state.searchDoctor === -1 || a.session.doctor.id === this.state.searchDoctor) &&
                        (this.state.searchDate === null || new Date(a.session.date).toLocaleDateString() ===
                            new Date(this.state.searchDate).toLocaleDateString()) &&
                        a.patient.name.toLowerCase().includes(this.state.searchPatient.toString().toLowerCase()) &&
                        (this.state.searchStatus === -1 ?true: this.state.searchStatus !== 0 ? a.confirmed : !a.confirmed)

                    )

                })
        })
    }

    clearFilters() {
        this.setState({
            searchDoctor: -1,
            searchStatus: -1,
            searchPatient: '',
            searchDate: null,
            filteredAppointments: this.state.appointments
        })
    }


    render() {
        return (

            <div className="app-wrapper h-100">
                <div className="page-heading mb-3 py-3">
                    <div className="d-flex flex-row">
                        <h2 className="title my-auto">Appointments</h2>
                        <div className="my-auto ml-auto d-flex">
                            <TextField
                                id="patientName"
                                label={"Patient"}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                value={this.state.searchPatient}
                                onChange={(event) => this.setState({searchPatient: event.target.value}, this.filterAppointments)}
                                margin="normal"
                                type="text"
                                className="mb-0 mt-auto"
                            />
                            <FormControl className="ml-3 mt-auto">
                                <span className="text-light-grey font-size-12">Doctor</span>
                                <Select
                                    value={this.state.searchDoctor}
                                    onChange={(event) => this.setState({searchDoctor: event.target.value}, this.filterAppointments)}
                                    autoWidth
                                >
                                    <MenuItem value={-1}>All</MenuItem>
                                    {this.state.doctors.map((d, index) => {
                                        return (
                                            <MenuItem key={index} value={d.id}>{d.name}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </FormControl>
                            <FormControl className="ml-3 mt-auto">
                                <DatePickers noPadding={true} label="Date" value={this.state.searchDate}
                                             onChange={(event) => {
                                                 this.setState({searchDate: event}, this.filterAppointments)
                                             }} maxDate={new Date()}/>
                            </FormControl>
                            <FormControl className="ml-3 mt-auto">
                                <span className="text-light-grey font-size-12">Status</span>
                                <Select
                                    value={this.state.searchStatus}
                                    onChange={(event) => this.setState({searchStatus: event.target.value}, this.filterAppointments)}
                                    autoWidth
                                >
                                    <MenuItem value={-1}>Any</MenuItem>
                                    {this.statuses.map((s, index) => {
                                        return (
                                            <MenuItem key={index} value={s.value}>{s.label}</MenuItem>
                                        )
                                    })}
                                </Select>
                            </FormControl>
                            <Button className="btn-lg ml-3 mt-auto mb-0" color={"secondary"} onClick={() => {
                                this.clearFilters()
                            }}>Clear Filters</Button>
                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="jr-card jr-card-widget jr-card-full card px-3 py-3">
                            <AppointmentTable data={this.state.filteredAppointments}
                                              onDataChange={() => this.getAllAppointments()}/>
                            <NotificationContainer/>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Appointments;
