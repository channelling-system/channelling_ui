import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import axios from "../../../util/Api";
import {NotificationManager} from 'react-notifications';
import Tooltip from "@material-ui/core/Tooltip";


class AppointmentTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            anchorEl: undefined,
            open: false,
            roomInfoOpen: false,
            items: this.props.data,
            roomToBeEdited: null
        };
        // this.items = this.props.data;
        this.saveChanges = this.saveChanges.bind(this)

    }


    handleClick = (event, n) => {
        this.setState({open: true, anchorEl: event.currentTarget, roomToBeEdited: n});
    };

    handleRequestClose = () => {
        this.setState({open: false});
    };

    openRoomInfoPopup = () => {
        this.setState({open: false, roomInfoOpen: true});
    };

    closeRoomInfoPopup = () => {
        this.setState({roomInfoOpen: false});
    };

    getAppointmentTime(data) {
        let session = data.session;
        let date = session.date;
        let start = session.start;
        let consultationTime = session.doctor.consultationTime;

        let startDateTime = new Date(date + ' ' + start);
        startDateTime.setMinutes(startDateTime.getMinutes() + consultationTime * (data.number - 1));
        return startDateTime.toLocaleTimeString(navigator.language, {
            hour: '2-digit',
            minute: '2-digit', hour12: false
        });
    }

    render() {
        return (
            <div className="table-container table-responsive-material">
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell className="table-header-cell-sticky" align="left">No</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Patient Name</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Date</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Time</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Doctor</TableCell>
                            <TableCell className="table-header-cell-sticky" align="center">Status</TableCell>
                            <TableCell className="table-header-cell-sticky" align="left">Room</TableCell>
                            {/*<TableCell className="table-header-cell-sticky" align="right">Action</TableCell>*/}
                            {/*doctor, date, time, status, room, appointment number*/}
                        </TableRow>
                    </TableHead>
                    <TableBody>

                        {this.props.data.map((data, index) => (
                            <TableRow key={index}>
                                <TableCell>{data.number}</TableCell>
                                <TableCell align="left">{data.patient.name}</TableCell>
                                <TableCell align="left">{data.session.date}</TableCell>
                                <TableCell align="left">{this.getAppointmentTime(data)}</TableCell>
                                <TableCell align="left">{data.session.doctor.name}</TableCell>
                                <TableCell align="center">
                                    <Tooltip title={data.confirmed? 'Confirmed': 'Not Confirmed'} placement="bottom-start">
                                        <i className={`zmdi zmdi-hc-fw jr-fs-xl zmdi-hc-2x align-self-center ${data.confirmed ? 'zmdi-check-circle text-success' : 'zmdi-close-circle text-orange'}`}/>
                                    </Tooltip>
                                </TableCell>
                                <TableCell align="left">{data.session.room.name}</TableCell>
                                {/*<TableCell align="right">*/}
                                {/*    <IconButton className="text-light icon-btn"*/}
                                {/*                onClick={(event) => this.handleClick(event, data)}*/}
                                {/*    >*/}
                                {/*        <i className="zmdi zmdi-more-vert text-grey"/>*/}
                                {/*    </IconButton>*/}
                                {/*    <Menu*/}
                                {/*        elevation={1}*/}
                                {/*        position={`left`}*/}
                                {/*        anchorEl={this.state.anchorEl}*/}
                                {/*        open={this.state.open}*/}
                                {/*        onClose={this.handleRequestClose}*/}
                                {/*    >*/}
                                {/*        <MenuItem onClick={this.openRoomInfoPopup}>Edit</MenuItem>*/}
                                {/*    </Menu>*/}
                                {/*</TableCell>*/}
                            </TableRow>
                        ))}


                    </TableBody>
                </Table>
            </div>
        );
    }

    saveChanges(e) {
        this.updateSession(e.id, e);
    }

    updateSession(id, data) {
        axios.put('rooms/' + id, data
        ).then(({data}) => {
           this.props.onDataChange();
            this.setState({roomInfoOpen: false});
            NotificationManager.success('Room updated successfully')
        }).catch(function (error) {
            NotificationManager.error('Room update failed');
            console.log("Error****:", error.message);
        });
    }


}


export default AppointmentTable;
